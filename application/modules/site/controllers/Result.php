<?php
class Result extends MY_Controller
{
  public function __construct()
  {
    parent::__construct();
    if(!IsLogin()) {
        redirect('site/user/login');
    }
  }

  public function index() {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN&&$ruser[COL_ROLEID]!=ROLEUSER) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }

    $data['title'] = "Hasil Perhitungan Suara";
    $data['res'] = $this->db
    ->where(COL_ISDELETED, 0)
    ->order_by(COL_TANGGAL, 'desc')
    ->get(TBL_TPEMILU)
    ->result_array();
    $this->template->load('backend' , 'result/index', $data);
  }

  public function detail($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN&&$ruser[COL_ROLEID]!=ROLEUSER) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }

    $data['rpemilu'] = $rpemilu = $this->db->where(COL_UNIQ, $id)->get(TBL_TPEMILU)->row_array();
    if(empty($rpemilu)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    $data['title'] = $rpemilu[COL_JUDUL];
    $this->template->load('backend' , 'result/detail', $data);
  }

  public function detail_partial($id) {
    $idKategori = $this->input->post('idKategori');
    $idKecamatan = $this->input->post('idKecamatan');
    $idKelurahan = $this->input->post('idKelurahan');

    $data['idKategori'] = $idKategori;
    $data['idKecamatan'] = $idKecamatan;
    $data['idKelurahan'] = $idKelurahan;
    $data['rtps'] = $rtps = $this->db
    ->where(COL_IDPEMILU, $id)
    ->where(COL_IDKELURAHAN, $idKelurahan)
    ->get(TBL_TPEMILUTPS)
    ->result_array();

    $this->load->view('result/detail-partial', $data);
  }

  public function form($idKategori, $idTPS) {
    $tab = $this->input->get('tab');
    $ruser = GetLoggedUser();
    $rtps = $this->db
    ->select('tpemilutps.*, mkelurahan.IdKecamatan')
    ->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_UNIQ." = ".TBL_TPEMILUTPS.".".COL_IDKELURAHAN,"left")
    ->where(TBL_TPEMILUTPS.'.'.COL_UNIQ, $idTPS)
    ->get(TBL_TPEMILUTPS)
    ->row_array();
    if(empty($rtps)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    $rdata = $this->db
    ->where(COL_IDPEMILU, $rtps[COL_IDPEMILU])
    ->where(COL_IDKATEGORI, $idKategori)
    ->where(COL_IDTPS, $idTPS)
    ->get(TBL_THASIL)
    ->row_array();

    $rkategori = $this->db
    ->where(COL_UNIQ, $idKategori)
    ->get(TBL_MKATEGORI)
    ->row_array();

    if(!empty($_POST)) {
      if(empty($tab) || $tab == 'info') {
        // Update if exists
        if(!empty($rdata)) {
          $dat = array(
            COL_JLH_DPT_PRIA=>$this->input->post(COL_JLH_DPT_PRIA),
            COL_JLH_DPT_WANITA=>$this->input->post(COL_JLH_DPT_WANITA),
            COL_JLH_DPTPEMILIH_PRIA=>$this->input->post(COL_JLH_DPTPEMILIH_PRIA),
            COL_JLH_DPTPEMILIH_WANITA=>$this->input->post(COL_JLH_DPTPEMILIH_WANITA),
            COL_JLH_DPTBPEMILIH_PRIA=>$this->input->post(COL_JLH_DPTBPEMILIH_PRIA),
            COL_JLH_DPTBPEMILIH_WANITA=>$this->input->post(COL_JLH_DPTBPEMILIH_WANITA),
            COL_JLH_DPKPEMILIH_PRIA=>$this->input->post(COL_JLH_DPKPEMILIH_PRIA),
            COL_JLH_DPKPEMILIH_WANITA=>$this->input->post(COL_JLH_DPKPEMILIH_WANITA),
            COL_JLH_DISABLITAS_PRIA=>$this->input->post(COL_JLH_DISABLITAS_PRIA),
            COL_JLH_DISABILITAS_WANITA=>$this->input->post(COL_JLH_DISABILITAS_WANITA),
            COL_JLH_SS_DITERIMA=>$this->input->post(COL_JLH_SS_DITERIMA),
            COL_JLH_SS_DIGUNAKAN=>$this->input->post(COL_JLH_SS_DIGUNAKAN),
            COL_JLH_SS_DIKEMBALIKAN=>$this->input->post(COL_JLH_SS_DIKEMBALIKAN),
            COL_JLH_SS_SISA=>$this->input->post(COL_JLH_SS_SISA),
            COL_UPDATEDBY=>$ruser[COL_USERNAME],
            COL_UPDATEDON=>date('Y-m-d H:i:s')
          );

          $this->db->trans_begin();
          try {
            $res = $this->db->where(COL_UNIQ, $rdata[COL_UNIQ])->update(TBL_THASIL, $dat);
            if(!$res) {
              $err = $this->db->error();
              throw new Exception($err['message']);
            }
          } catch(Exception $ex) {
            $this->db->trans_rollback();
            ShowJsonError($ex->getMessage());
            exit();
          }

          $this->db->trans_commit();
          ShowJsonSuccess('Data berhasil diperbarui.', array('redirect'=>site_url('site/result/form/'.$idKategori.'/'.$idTPS).'?tab=detail'));
          exit();
        } else {
          $dat = array(
            COL_IDPEMILU=>$rtps[COL_IDPEMILU],
            COL_IDKATEGORI=>$idKategori,
            COL_IDTPS=>$idTPS,
            COL_JLH_DPT_PRIA=>$this->input->post(COL_JLH_DPT_PRIA),
            COL_JLH_DPT_WANITA=>$this->input->post(COL_JLH_DPT_WANITA),
            COL_JLH_DPTPEMILIH_PRIA=>$this->input->post(COL_JLH_DPTPEMILIH_PRIA),
            COL_JLH_DPTPEMILIH_WANITA=>$this->input->post(COL_JLH_DPTPEMILIH_WANITA),
            COL_JLH_DPTBPEMILIH_PRIA=>$this->input->post(COL_JLH_DPTBPEMILIH_PRIA),
            COL_JLH_DPTBPEMILIH_WANITA=>$this->input->post(COL_JLH_DPTBPEMILIH_WANITA),
            COL_JLH_DPKPEMILIH_PRIA=>$this->input->post(COL_JLH_DPKPEMILIH_PRIA),
            COL_JLH_DPKPEMILIH_WANITA=>$this->input->post(COL_JLH_DPKPEMILIH_WANITA),
            COL_JLH_DISABLITAS_PRIA=>$this->input->post(COL_JLH_DISABLITAS_PRIA),
            COL_JLH_DISABILITAS_WANITA=>$this->input->post(COL_JLH_DISABILITAS_WANITA),
            COL_JLH_SS_DITERIMA=>$this->input->post(COL_JLH_SS_DITERIMA),
            COL_JLH_SS_DIGUNAKAN=>$this->input->post(COL_JLH_SS_DIGUNAKAN),
            COL_JLH_SS_DIKEMBALIKAN=>$this->input->post(COL_JLH_SS_DIKEMBALIKAN),
            COL_JLH_SS_SISA=>$this->input->post(COL_JLH_SS_SISA),
            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );

          $this->db->trans_begin();
          try {
            $res = $this->db->insert(TBL_THASIL, $dat);
            if(!$res) {
              $err = $this->db->error();
              throw new Exception($err['message']);
            }
          } catch(Exception $ex) {
            $this->db->trans_rollback();
            ShowJsonError($ex->getMessage());
            exit();
          }

          $this->db->trans_commit();
          ShowJsonSuccess('Data berhasil ditambahkan.', array('redirect'=>site_url('site/result/form/'.$idKategori.'/'.$idTPS).'?tab=detail'));
          exit();
        }
      } else if($tab == 'detail') {
        $arrHasil = json_decode($this->input->post('ArrHasil'));
        $dat = array();
        foreach($arrHasil as $arr) {
          $dat[] = array(
            COL_IDHASIL=>$rdata[COL_UNIQ],
            COL_IDPARTAI=>!empty($arr->IdPartai)?$arr->IdPartai:null,
            COL_IDKANDIDAT=>!empty($arr->IdKandidat)?$arr->IdKandidat:null,
            COL_JLHSUARA=>$arr->JlhSuara,
            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );
        }

        $this->db->trans_begin();
        try {
          $res = $this->db->where(COL_IDHASIL, $rdata[COL_UNIQ])->delete(TBL_THASILDET);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $res = $this->db->insert_batch(TBL_THASILDET, $dat);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Data berhasil diperbarui.', array('redirect'=>site_url('site/result/form/'.$idKategori.'/'.$idTPS).'?tab=total'));
        exit();
      } else if($tab == 'total') {
        $dat = array(
          COL_JLH_SUARA_SAH=>$this->input->post(COL_JLH_SUARA_SAH),
          COL_JLH_SUARA_TIDAKSAH=>$this->input->post(COL_JLH_SUARA_TIDAKSAH)
        );

        $this->db->trans_begin();
        try {
          $res = $this->db->where(COL_UNIQ, $rdata[COL_UNIQ])->update(TBL_THASIL, $dat);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Data berhasil diperbarui.', array('redirect'=>site_url('site/result/form/'.$idKategori.'/'.$idTPS).'?tab=doc'));
        exit();
      } else if($tab == 'doc') {

      }
    } else {
      $data['title'] = 'Rincian Hasil Perhitungan';
      $data['rkategori'] = $rkategori;
      $data['rtps'] = $rtps;
      $data['rdata'] = $rdata;
      $this->template->load('backend' , 'result/form', $data);
    }
  }

  public function doc_add($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_THASIL)
    ->row_array();

    if(empty($rdata)) {
      echo 'Parameter tidak valid.';
      exit();
    }

    if(!empty($_POST)) {
      $nmfile = null;
      if (!empty($_FILES['userfile']['name'])) {
        $config['upload_path'] = MY_UPLOADPATH.'hasil/';
        $config['allowed_types'] = "jpg|jpeg|png|pdf";
        $config['max_size']	= 10240;
        $config['overwrite'] = FALSE;

        $this->load->library('upload',$config);
        $res = $this->upload->do_upload('userfile');
        if(!$res) {
          $err = $this->upload->display_errors('', '');
          ShowJsonError($err);
          exit();
        }
        $upl = $this->upload->data();
        $nmfile = $upl['file_name'];
      }

      $dat = array(
        COL_IDHASIL=>$id,
        COL_DOCNAME=>$this->input->post(COL_DOCNAME),
        COL_DOCURL=>$nmfile,
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_THASILDOC, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $this->load->view('result/form-doc');
    }
  }

  public function doc_delete($id) {
    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_THASILDOC)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_THASILDOC, array(COL_ISDELETED=>1));
    if(!$res) {
      ShowJsonError('Gagal menghapus data.');
      exit();
    }

    ShowJsonSuccess('Berhasil menghapus data.');
    exit();
  }
}
