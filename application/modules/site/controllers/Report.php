<?php
class Report extends MY_Controller
{
  public function __construct()
  {
    parent::__construct();
    if(!IsLogin()) {
        redirect('site/user/login');
    }
  }

  public function hasil() {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN&&$ruser[COL_ROLEID]!=ROLEUSER) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }

    if(!empty($_POST)) {
      $idPemilu = $this->input->post('idPemilu');
      $idKategori = $this->input->post('idKategori');
      $idKecamatan = $this->input->post('idKecamatan');
      $idKelurahan = $this->input->post('idKelurahan');

      $rkategori = $this->db->where(COL_UNIQ, $idKategori)->get(TBL_MKATEGORI)->row_array();
      $rkecamatan = array();
      if(!empty($idKecamatan)) {
        $rkecamatan = $this->db->where(COL_UNIQ, $idKecamatan)->get(TBL_MKECAMATAN)->row_array();
      }

      $data['idPemilu'] = $idPemilu;
      $data['idKategori'] = $idKategori;
      $data['idKecamatan'] = $idKecamatan;
      $data['idKelurahan'] = $idKelurahan;
      $data['rkategori'] = $rkategori;
      $data['rkecamatan'] = $rkecamatan;

      /*if($rkategori[COL_KATNAMA]=='PPWP' || $rkategori[COL_KATNAMA]=='DPD') {
        $this->db->order_by(COL_KANDNO);
      } else {
        $this->db->order_by('COALESCE(ParNoUrut, (select mpartai.ParNoUrut from tkandidatpartai left join mpartai on mpartai.Uniq = tkandidatpartai.IdPartai where tkandidatpartai.IdKandidat = tkandidat.Uniq limit 1), KandNo), COALESCE(KandNo, 0) asc');
      }

      $rhasil = $this->db
      ->select('
        mpartai.ParNama,
        mpartai.ParID,
        mpartai.ParNoUrut,
        tkandidat.KandNama,
        tkandidat.KandNamaWakil,
        sum(JlhSuara) as JlhSuara,
        COALESCE(ParNoUrut, KandNo) as NoUrut,
        COALESCE(CONCAT(ParID, " - ", ParNama), KandNama) as Nama
        ')
      ->join(TBL_THASIL,TBL_THASIL.'.'.COL_UNIQ." = ".TBL_THASILDET.".".COL_IDHASIL,"left")
      ->join(TBL_TPEMILUTPS,TBL_TPEMILUTPS.'.'.COL_UNIQ." = ".TBL_THASIL.".".COL_IDTPS,"left")
      ->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_UNIQ." = ".TBL_TPEMILUTPS.".".COL_IDKELURAHAN,"left")
      ->join(TBL_MPARTAI,TBL_MPARTAI.'.'.COL_UNIQ." = ".TBL_THASILDET.".".COL_IDPARTAI,"left")
      ->join(TBL_TKANDIDAT,TBL_TKANDIDAT.'.'.COL_UNIQ." = ".TBL_THASILDET.".".COL_IDKANDIDAT,"left")
      ->where(TBL_THASIL.'.'.COL_IDPEMILU, $idPemilu)
      ->where(TBL_THASIL.'.'.COL_IDKATEGORI, $idKategori)
      ->group_by(TBL_THASILDET.'.'.COL_IDPARTAI)
      ->group_by(TBL_THASILDET.'.'.COL_IDKANDIDAT)
      ->get(TBL_THASILDET)
      ->result_array();*/

      if(!empty($idKecamatan)) {
        $this->db->where(COL_IDKECAMATAN, $idKecamatan);
        if(!empty($idKelurahan)) {
          $this->db->where(COL_IDKELURAHAN, $idKelurahan);
        }
      }

      $rhasil = $this->db
      ->select('sum(Jlh_Suara_Sah) as Jlh_Suara_Sah, count(IdTPS) as Jlh_TPS')
      ->join(TBL_TPEMILUTPS,TBL_TPEMILUTPS.'.'.COL_UNIQ." = ".TBL_THASIL.".".COL_IDTPS,"left")
      ->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_UNIQ." = ".TBL_TPEMILUTPS.".".COL_IDKELURAHAN,"left")
      ->where(TBL_THASIL.'.'.COL_IDPEMILU, $idPemilu)
      ->where(TBL_THASIL.'.'.COL_IDKATEGORI, $idKategori)
      ->get(TBL_THASIL)
      ->row_array();

      if(!empty($idKecamatan)) {
        $this->db->where(COL_IDKECAMATAN, $idKecamatan);
        if(!empty($idKelurahan)) {
          $this->db->where(COL_IDKELURAHAN, $idKelurahan);
        }
      }
      $numTPS = $this->db
      ->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_UNIQ." = ".TBL_TPEMILUTPS.".".COL_IDKELURAHAN,"left")
      ->where(TBL_TPEMILUTPS.'.'.COL_IDPEMILU, $idPemilu)
      ->count_all_results(TBL_TPEMILUTPS);

      $data['rhasil'] = $rhasil;
      $data['numTPS'] = $numTPS;
      $this->load->view('report/hasil-partial', $data);

    } else {
      $data['title'] = 'HASIL PERHITUNGAN';
      $this->template->load('backend' , 'report/hasil', $data);
    }


  }
}
