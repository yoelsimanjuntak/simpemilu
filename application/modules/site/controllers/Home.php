<?php
class Home extends MY_Controller {

  public function mazer() {
    $data['title'] = 'Mazer Template';
    $this->template->load('mazer' , 'home/mazer', $data);
  }

  public function index() {
    $data['title'] = $this->setting_web_desc;
    $this->template->load('frontend' , 'home/index', $data);
  }

  public function pemilu_detail($id) {
    $rpemilu = $this->db->where(COL_UNIQ, $id)->get(TBL_TPEMILU)->row_array();
    if(empty($rpemilu)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    $data['title'] = 'Hasil Perhitungan';
    $data['rpemilu'] = $rpemilu;
    $this->template->load('frontend' , 'home/pemilu-detail', $data);
  }

  public function lapor() {
    $data['title'] = 'Pengaduan';
    if(!empty($_POST)) {
      $dat = array(
        COL_LAPORNAMA=>$this->input->post(COL_LAPORNAMA),
        COL_LAPORNIK=>$this->input->post(COL_LAPORNIK),
        COL_LAPORHP=>$this->input->post(COL_LAPORHP),
        COL_LAPOREMAIL=>$this->input->post(COL_LAPOREMAIL),
        COL_LAPORISI=>$this->input->post(COL_LAPORISI),
        COL_LAPORKATEGORI=>$this->input->post(COL_LAPORKATEGORI),
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TLAPOR, $dat);
        if(!$res) {
          throw new Exception('Mohon maaf, sedang terjadi kendala pada sistem kami. Silakan mencoba beberapa saat lagi.');
        }
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }

      $this->db->trans_commit();
      ShowJsonSuccess('Laporan anda telah diterima. Kami akan segera menindaklanjuti laporan yang anda kirimkan. Terimakasih atas partisipasi anda!');
      exit();
    } else {
      $this->template->load('gotto' , 'home/lapor', $data);
    }

  }
}
 ?>
