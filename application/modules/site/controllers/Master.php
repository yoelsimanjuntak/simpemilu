<?php
class Master extends MY_Controller
{
  public function __construct()
  {
    parent::__construct();
    if(!IsLogin()) {
        redirect('site/user/login');
    }
    if(GetLoggedUser()[COL_ROLEID]!=ROLEADMIN) {
      show_error('Anda tidak memiliki akses terhadap modul ini.');
      exit();
    }
  }

  public function kabkota() {
    $data['title'] = "Kabupaten / Kota";
    $data['res'] = $this->db
    ->where(COL_ISDELETED, 0)
    ->get(TBL_MKABUPATEN)
    ->result_array();
    $this->template->load('backend' , 'master/kabkota', $data);
  }

  public function kabkota_add() {
    if(!empty($_POST)) {
      $dat = array(
        COL_KABUPATEN=>$this->input->post(COL_KABUPATEN)
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_MKABUPATEN, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $this->load->view('master/kabkota-form');
    }
  }

  public function kabkota_edit($id) {
    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_MKABUPATEN)->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_KABUPATEN=>$this->input->post(COL_KABUPATEN)
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_MKABUPATEN, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil memperbarui data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['data'] = $rdata;
      $this->load->view('master/kabkota-form', $data);
    }
  }

  public function kabkota_delete($id) {
    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_MKABUPATEN)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_MKABUPATEN, array(COL_ISDELETED=>1));
    if(!$res) {
      ShowJsonError('Gagal menghapus data.');
      exit();
    }

    ShowJsonSuccess('Berhasil menghapus data.');
    exit();
  }

  public function kecamatan() {
    $data['title'] = "Kecamatan";
    $this->template->load('backend' , 'master/kecamatan', $data);
  }

  public function kecamatan_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $filterKabkota = !empty($_POST['filterKabkota'])?$_POST['filterKabkota']:null;

    $ruser = GetLoggedUser();
    $orderables = array(null,COL_KABUPATEN,COL_KECAMATAN);
    $cols = array(COL_KABUPATEN,COL_KECAMATAN);

    $queryAll = $this->db->where(COL_ISDELETED, 0)->get(TBL_MKECAMATAN);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }
    if(!empty($filterKabkota)) {
      $this->db->where(COL_IDKABUPATEN, $filterKabkota);
    }

    if(!empty($_POST['order'])){
      $order = $orderables[$_POST['order']['0']['column']];
      $this->db->order_by($order, $_POST['order']['0']['dir']);
    }else {
      $this->db->order_by(COL_KABUPATEN, 'asc');
      $this->db->order_by(COL_KECAMATAN, 'asc');
    }

    $q = $this->db
    ->select("mkecamatan.*, mkabupaten.Kabupaten, mdapil.Dapil as DapilNama")
    ->join(TBL_MKABUPATEN,TBL_MKABUPATEN.'.'.COL_UNIQ." = ".TBL_MKECAMATAN.".".COL_IDKABUPATEN,"inner")
    ->join(TBL_MDAPIL,TBL_MDAPIL.'.'.COL_UNIQ." = ".TBL_MKECAMATAN.".".COL_DAPIL,"inner")
    ->where(TBL_MKECAMATAN.'.'.COL_ISDELETED, 0)
    ->get_compiled_select(TBL_MKECAMATAN, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/master/kecamatan-edit/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-primary btn-edit"><i class="fas fa-edit"></i></a>&nbsp;';
      $htmlBtn .= '<a href="'.site_url('site/master/kecamatan-delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-danger btn-action"><i class="fas fa-times-circle"></i></a>';

      $data[] = array(
        $htmlBtn,
        $r[COL_KABUPATEN],
        $r[COL_KECAMATAN],
        $r['DapilNama']
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function kecamatan_add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $dat = array(
        COL_IDKABUPATEN=>$this->input->post(COL_IDKABUPATEN),
        COL_KECAMATAN=>$this->input->post(COL_KECAMATAN),
        COL_DAPIL=>$this->input->post(COL_DAPIL)
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_MKECAMATAN, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }

      $this->db->trans_commit();
      ShowJsonSuccess('Data berhasil ditambahkan.', array('redirect'=>site_url('site/master/kecamatan')));
      exit();
    } else {
      $this->load->view('master/kecamatan-form');
    }
  }

  public function kecamatan_edit($id) {
    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_MKECAMATAN)->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_IDKABUPATEN=>$this->input->post(COL_IDKABUPATEN),
        COL_KECAMATAN=>$this->input->post(COL_KECAMATAN),
        COL_DAPIL=>$this->input->post(COL_DAPIL)
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_MKECAMATAN, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil memperbarui data.', array('redirect'=>site_url('site/master/kecamatan')));
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['data'] = $rdata;
      $this->load->view('master/kecamatan-form', $data);
    }
  }

  public function kecamatan_delete($id) {
    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_MKECAMATAN)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_MKECAMATAN, array(COL_ISDELETED=>1));
    if(!$res) {
      ShowJsonError('Gagal menghapus data.');
      exit();
    }

    ShowJsonSuccess('Berhasil menghapus data.');
    exit();
  }

  public function kelurahan() {
    $data['title'] = "Kelurahan";
    $this->template->load('backend' , 'master/kelurahan', $data);
  }

  public function kelurahan_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $filterKabkota = !empty($_POST['filterKabkota'])?$_POST['filterKabkota']:null;
    $filterKecamatan = !empty($_POST['filterKecamatan'])?$_POST['filterKecamatan']:null;

    $ruser = GetLoggedUser();
    $orderables = array(null,COL_KABUPATEN,COL_KECAMATAN,COL_KELURAHAN);
    $cols = array(COL_KABUPATEN,COL_KECAMATAN,COL_KELURAHAN);

    $queryAll = $this->db->where(COL_ISDELETED, 0)->get(TBL_MKELURAHAN);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }
    if(!empty($filterKabkota)) {
      $this->db->where(TBL_MKECAMATAN.'.'.COL_IDKABUPATEN, $filterKabkota);
      if(!empty($filterKecamatan)) {
        $this->db->where(TBL_MKELURAHAN.'.'.COL_IDKECAMATAN, $filterKecamatan);
      }
    }

    if(!empty($_POST['order'])){
      $order = $orderables[$_POST['order']['0']['column']];
      $this->db->order_by($order, $_POST['order']['0']['dir']);
    }else {
      $this->db->order_by(COL_KABUPATEN, 'asc');
      $this->db->order_by(COL_KECAMATAN, 'asc');
      $this->db->order_by(COL_KELURAHAN, 'asc');
    }

    $q = $this->db
    ->select("mkelurahan.*, mkabupaten.Kabupaten, mkecamatan.Kecamatan")
    ->join(TBL_MKECAMATAN,TBL_MKECAMATAN.'.'.COL_UNIQ." = ".TBL_MKELURAHAN.".".COL_IDKECAMATAN,"inner")
    ->join(TBL_MKABUPATEN,TBL_MKABUPATEN.'.'.COL_UNIQ." = ".TBL_MKECAMATAN.".".COL_IDKABUPATEN,"inner")
    ->where(TBL_MKELURAHAN.'.'.COL_ISDELETED, 0)
    ->get_compiled_select(TBL_MKELURAHAN, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/master/kelurahan-edit/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-primary btn-edit"><i class="fas fa-edit"></i></a>&nbsp;';
      $htmlBtn .= '<a href="'.site_url('site/master/kelurahan-delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-danger btn-action"><i class="fas fa-times-circle"></i></a>';

      $data[] = array(
        $htmlBtn,
        $r[COL_KABUPATEN],
        $r[COL_KECAMATAN],
        $r[COL_KELURAHAN]
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function kelurahan_add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $dat = array(
        COL_IDKECAMATAN=>$this->input->post(COL_IDKECAMATAN),
        COL_KELURAHAN=>$this->input->post(COL_KELURAHAN)
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_MKELURAHAN, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }

      $this->db->trans_commit();
      ShowJsonSuccess('Data berhasil ditambahkan.', array('redirect'=>site_url('site/master/kelurahan')));
      exit();
    } else {
      $this->load->view('master/kelurahan-form');
    }
  }

  public function kelurahan_edit($id) {
    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_MKELURAHAN)->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_IDKECAMATAN=>$this->input->post(COL_IDKECAMATAN),
        COL_KELURAHAN=>$this->input->post(COL_KELURAHAN)
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_MKELURAHAN, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil memperbarui data.', array('redirect'=>site_url('site/master/kelurahan')));
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['data'] = $rdata;
      $this->load->view('master/kelurahan-form', $data);
    }
  }

  public function kelurahan_delete($id) {
    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_MKELURAHAN)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_MKELURAHAN, array(COL_ISDELETED=>1));
    if(!$res) {
      ShowJsonError('Gagal menghapus data.');
      exit();
    }

    ShowJsonSuccess('Berhasil menghapus data.');
    exit();
  }

  public function partai() {
    $data['title'] = "Partai Politik";
    $data['res'] = $this->db
    ->order_by(COL_PARNOURUT, 'asc')
    ->get(TBL_MPARTAI)
    ->result_array();
    $this->template->load('backend' , 'master/partai', $data);
  }

  public function partai_add() {
    if(!empty($_POST)) {
      $nmfile = null;
      if (!empty($_FILES['userfile']['name'])) {
        $config['upload_path'] = MY_UPLOADPATH.'partai/';
        $config['allowed_types'] = "jpg|jpeg|png";
        $config['max_size']	= 10240;
        $config['overwrite'] = FALSE;

        $this->load->library('upload',$config);
        $res = $this->upload->do_upload('userfile');
        if(!$res) {
          $err = $this->upload->display_errors('', '');
          ShowJsonError($err);
          exit();
        }
        $upl = $this->upload->data();
        $nmfile = $upl['file_name'];
      }

      $dat = array(
        COL_PARNAMA=>$this->input->post(COL_PARNAMA),
        COL_PARID=>$this->input->post(COL_PARID),
        COL_PARNOURUT=>$this->input->post(COL_PARNOURUT),
        COL_PARLOGO=>$nmfile
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_MPARTAI, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $this->load->view('master/partai-form');
    }
  }

  public function partai_edit($id) {
    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_MPARTAI)->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    if(!empty($_POST)) {
      $nmfile = $rdata[COL_PARLOGO];
      if (!empty($_FILES['userfile']['name'])) {
        $config['upload_path'] = MY_UPLOADPATH.'partai/';
        $config['allowed_types'] = "jpg|jpeg|png";
        $config['max_size']	= 10240;
        $config['overwrite'] = FALSE;

        $this->load->library('upload',$config);
        $res = $this->upload->do_upload('userfile');
        if(!$res) {
          $err = $this->upload->display_errors('', '');
          ShowJsonError($err);
          exit();
        }
        $upl = $this->upload->data();
        $nmfile = $upl['file_name'];
      }

      $dat = array(
        COL_PARNAMA=>$this->input->post(COL_PARNAMA),
        COL_PARID=>$this->input->post(COL_PARID),
        COL_PARNOURUT=>$this->input->post(COL_PARNOURUT),
        COL_PARLOGO=>$nmfile
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_MPARTAI, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil memperbarui data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['data'] = $rdata;
      $this->load->view('master/partai-form', $data);
    }
  }

  public function get_opt_kecamatan($id, $def='') {
    $rkecamatan = $this->db
    ->query("select * from mkecamatan where IdKabupaten=$id order by Kecamatan asc")
    ->result_array();

    $opt = '';
    if($def!='noempty') $opt .= '<option value="">- SEMUA KECAMATAN -</option>';

    foreach($rkecamatan as $kec) {
      $opt .= '<option value="'.$kec[COL_UNIQ].'" data-url="'.site_url('site/master/get-opt-kelurahan/'.$kec[COL_UNIQ]).'">'.$kec[COL_KECAMATAN].'</option>';
    }
    echo $opt;
  }

  public function get_opt_kelurahan($id, $def='') {
    $rkelurahan = $this->db
    ->query("select * from mkelurahan where IdKecamatan=$id order by Kelurahan asc")
    ->result_array();

    $opt = '';
    if($def!='noempty') $opt .= '<option value="">- SEMUA KELURAHAN -</option>';

    foreach($rkelurahan as $kel) {
      $opt .= '<option value="'.$kel[COL_UNIQ].'">'.$kel[COL_KELURAHAN].'</option>';
    }
    echo $opt;
  }
}
