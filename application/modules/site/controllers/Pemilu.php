<?php
class Pemilu extends MY_Controller
{
  public function __construct()
  {
    parent::__construct();
    if(!IsLogin()) {
        redirect('site/user/login');
    }
  }

  public function index() {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }

    $data['title'] = "Jadwal Pemilihan Umum";
    $data['res'] = $this->db
    ->where(COL_ISDELETED, 0)
    ->order_by(COL_TANGGAL, 'desc')
    ->get(TBL_TPEMILU)
    ->result_array();
    $this->template->load('backend' , 'pemilu/index', $data);
  }

  public function add() {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }
    if(!empty($_POST)) {
      $arrDet = $this->input->post('ArrDet');
      $det = array();
      $dat = array(
        COL_JUDUL=>$this->input->post(COL_JUDUL),
        COL_TANGGAL=>date('Y-m-d', strtotime($this->input->post(COL_TANGGAL))),
        COL_PUNGUTMULAI=>date('Y-m-d', strtotime($this->input->post(COL_TANGGAL))).' '.$this->input->post(COL_PUNGUTMULAI).':00',
        COL_PUNGUTAKHIR=>date('Y-m-d', strtotime($this->input->post(COL_TANGGAL))).' '.$this->input->post(COL_PUNGUTAKHIR).':00',
        COL_HITUNGMULAI=>date('Y-m-d', strtotime($this->input->post(COL_TANGGAL))).' '.$this->input->post(COL_HITUNGMULAI).':00',
        COL_HITUNGAKHIR=>date('Y-m-d', strtotime($this->input->post(COL_TANGGAL))).' '.$this->input->post(COL_HITUNGAKHIR).':00',
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TPEMILU, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $idPemilu = $this->db->insert_id();
        if(!empty($arrDet)) {
          foreach ($arrDet as $d) {
            $det[] = array(COL_IDPEMILU=>$idPemilu, COL_IDKATEGORI=>$d);
          }

          $res = $this->db->insert_batch(TBL_TPEMILUDET, $det);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $this->load->view('pemilu/form');
    }
  }

  public function edit($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }
    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_TPEMILU)->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    if(!empty($_POST)) {
      $arrDet = $this->input->post('ArrDet');
      $det = array();
      $dat = array(
        COL_JUDUL=>$this->input->post(COL_JUDUL),
        COL_TANGGAL=>date('Y-m-d', strtotime($this->input->post(COL_TANGGAL))),
        COL_PUNGUTMULAI=>date('Y-m-d', strtotime($this->input->post(COL_TANGGAL))).' '.$this->input->post(COL_PUNGUTMULAI).':00',
        COL_PUNGUTAKHIR=>date('Y-m-d', strtotime($this->input->post(COL_TANGGAL))).' '.$this->input->post(COL_PUNGUTAKHIR).':00',
        COL_HITUNGMULAI=>date('Y-m-d', strtotime($this->input->post(COL_TANGGAL))).' '.$this->input->post(COL_HITUNGMULAI).':00',
        COL_HITUNGAKHIR=>date('Y-m-d', strtotime($this->input->post(COL_TANGGAL))).' '.$this->input->post(COL_HITUNGAKHIR).':00',
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TPEMILU, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $res = $this->db->where(COL_IDPEMILU, $id)->delete(TBL_TPEMILUDET);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        if(!empty($arrDet)) {
          foreach ($arrDet as $d) {
            $det[] = array(COL_IDPEMILU=>$id, COL_IDKATEGORI=>$d);
          }

          $res = $this->db->insert_batch(TBL_TPEMILUDET, $det);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menambah data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['data'] = $rdata;
      $this->load->view('pemilu/form', $data);
    }
  }

  public function delete($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      ShowJsonError('Maaf, anda tidak memiliki hak akses.');
      exit();
    }

    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_TPEMILU)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TPEMILU, array(COL_ISDELETED=>1));
    if(!$res) {
      ShowJsonError('Gagal menghapus data.');
      exit();
    }

    ShowJsonSuccess('Berhasil menghapus data.');
    exit();
  }

  public function tps($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }

    $rpemilu = $this->db->where(COL_UNIQ, $id)->get(TBL_TPEMILU)->row_array();
    if(empty($rpemilu)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    $data['title'] = "Tempat Pemungutan Suara";
    $data['id'] = $id;
    $this->template->load('backend' , 'pemilu/tps', $data);
  }

  public function tps_load($id) {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $filterKabkota = !empty($_POST['filterKabkota'])?$_POST['filterKabkota']:null;
    $filterKecamatan = !empty($_POST['filterKecamatan'])?$_POST['filterKecamatan']:null;
    $filterKelurahan = !empty($_POST['filterKelurahan'])?$_POST['filterKelurahan']:null;

    $ruser = GetLoggedUser();
    $orderables = array(null,COL_KABUPATEN,COL_KECAMATAN,COL_KELURAHAN,COL_TPSNAMA);
    $cols = array(COL_KABUPATEN,COL_KECAMATAN,COL_KELURAHAN,COL_TPSNAMA,COL_TPSKETERANGAN);

    $queryAll = $this->db->where(COL_IDPEMILU, $id)->where(COL_ISDELETED, 0)->get(TBL_TPEMILUTPS);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }
    if(!empty($filterKabkota)) {
      $this->db->where(TBL_MKECAMATAN.'.'.COL_IDKABUPATEN, $filterKabkota);
      if(!empty($filterKecamatan)) {
        $this->db->where(TBL_MKELURAHAN.'.'.COL_IDKECAMATAN, $filterKecamatan);
        if(!empty($filterKelurahan)) {
          $this->db->where(TBL_TPEMILUTPS.'.'.COL_IDKELURAHAN, $filterKelurahan);
        }
      }
    }

    if(!empty($_POST['order'])){
      $order = $orderables[$_POST['order']['0']['column']];
      if($order==COL_TPSNAMA) {
        $this->db->order_by('CONVERT(TPSNama, DECIMAL) '.$_POST['order']['0']['dir']);
      } else {
        $this->db->order_by($order, $_POST['order']['0']['dir']);
      }

    }else {
      $this->db->order_by(COL_KABUPATEN, 'asc');
      $this->db->order_by(COL_KECAMATAN, 'asc');
      $this->db->order_by(COL_KELURAHAN, 'asc');
      $this->db->order_by('CONVERT(TPSNama, DECIMAL) asc');
    }

    $q = $this->db
    ->select("tpemilutps.*, mkabupaten.Kabupaten, mkecamatan.Kecamatan, mkelurahan.Kelurahan")
    ->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_UNIQ." = ".TBL_TPEMILUTPS.".".COL_IDKELURAHAN,"inner")
    ->join(TBL_MKECAMATAN,TBL_MKECAMATAN.'.'.COL_UNIQ." = ".TBL_MKELURAHAN.".".COL_IDKECAMATAN,"inner")
    ->join(TBL_MKABUPATEN,TBL_MKABUPATEN.'.'.COL_UNIQ." = ".TBL_MKECAMATAN.".".COL_IDKABUPATEN,"inner")
    ->where(TBL_TPEMILUTPS.'.'.COL_ISDELETED, 0)
    ->get_compiled_select(TBL_TPEMILUTPS, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/pemilu/tps-edit/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-primary btn-edit"><i class="fas fa-edit"></i></a>&nbsp;';
      $htmlBtn .= '<a href="'.site_url('site/pemilu/tps-delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-danger btn-action"><i class="fas fa-times-circle"></i></a>';

      $data[] = array(
        $htmlBtn,
        $r[COL_KABUPATEN],
        $r[COL_KECAMATAN],
        $r[COL_KELURAHAN],
        str_pad($r[COL_TPSNAMA],2,"0",STR_PAD_LEFT),
        $r[COL_TPSKETERANGAN]
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function tps_add($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }
    $rpemilu = $this->db->where(COL_UNIQ, $id)->get(TBL_TPEMILU)->row_array();
    if(empty($rpemilu)) {
      echo 'Parameter tidak valid.';
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_IDPEMILU=>$id,
        COL_IDKELURAHAN=>$this->input->post(COL_IDKELURAHAN),
        COL_TPSNAMA=>$this->input->post(COL_TPSNAMA),
        COL_TPSKOORDINAT=>$this->input->post(COL_TPSKOORDINAT),
        COL_TPSKETERANGAN=>$this->input->post(COL_TPSKETERANGAN),
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TPEMILUTPS, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }

      $this->db->trans_commit();
      ShowJsonSuccess('Data berhasil ditambahkan.', array('redirect'=>site_url('site/pemilu/tps/'.$id)));
      exit();
    } else {
      $this->load->view('pemilu/tps-form');
    }
  }

  public function tps_edit($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }
    $rdata = $this->db
    ->select('tpemilutps.*, mkelurahan.IdKecamatan')
    ->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_UNIQ." = ".TBL_TPEMILUTPS.".".COL_IDKELURAHAN,"inner")
    ->where(TBL_TPEMILUTPS.'.'.COL_UNIQ, $id)
    ->get(TBL_TPEMILUTPS)
    ->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    if(!empty($_POST)) {
      /* Checking existing hasil pemilu */
      $rhasil = $this->db
      ->where(COL_IDTPS, $id)
      ->get(TBL_THASIL)
      ->row_array();
      if(!empty($rhasil)) {
        if($rdata[COL_IDKELURAHAN]!=$this->input->post(COL_IDKELURAHAN)) {
          ShowJsonError('Maaf, informasi Kelurahan TPS '.$rdata[COL_TPSNAMA].' tidak dapat diubah dikarenakan sudah terdapat data hasil perhitungan sebelumnya pada TPS tersebut. Silakan hubungi Administrator Sistem untuk informasi lebih detil.');
          exit();
        }
      }
      /* Checking existing hasil pemilu */

      $dat = array(
        COL_IDPEMILU=>$id,
        COL_IDKELURAHAN=>$this->input->post(COL_IDKELURAHAN),
        COL_TPSNAMA=>$this->input->post(COL_TPSNAMA),
        COL_TPSKOORDINAT=>$this->input->post(COL_TPSKOORDINAT),
        COL_TPSKETERANGAN=>$this->input->post(COL_TPSKETERANGAN),
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TPEMILUTPS, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil memperbarui data.', array('redirect'=>site_url('site/pemilu/tps/'.$rdata[COL_IDPEMILU])));
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['data'] = $rdata;
      $this->load->view('pemilu/tps-form', $data);
    }
  }

  public function tps_delete($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      ShowJsonError('Maaf, anda tidak memiliki hak akses.');
      exit();
    }
    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_TPEMILUTPS)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TPEMILUTPS, array(COL_ISDELETED=>1));
    if(!$res) {
      ShowJsonError('Gagal menghapus data.');
      exit();
    }

    ShowJsonSuccess('Berhasil menghapus data.');
    exit();
  }

  public function calon($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }
    $rpemilu = $this->db->where(COL_UNIQ, $id)->get(TBL_TPEMILU)->row_array();
    if(empty($rpemilu)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    $data['title'] = "Daftar Calon";
    $data['id'] = $id;
    $this->template->load('backend' , 'pemilu/calon', $data);
  }

  public function calon_load($id) {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $filterKategori = !empty($_POST['filterKategori'])?$_POST['filterKategori']:null;
    $filterPartai = !empty($_POST['filterPartai'])?$_POST['filterPartai']:null;

    $ruser = GetLoggedUser();
    $orderables = array(null,null,COL_KANDNAMA,COL_DAPIL);
    $cols = array(COL_KANDNAMA,COL_KANDNAMAWAKIL,COL_DAPIL);

    $queryAll = $this->db->where(COL_IDPEMILU, $id)->where(COL_ISDELETED, 0)->get(TBL_TKANDIDAT);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }
    if(!empty($filterKategori)) {
      $this->db->where(TBL_TKANDIDAT.'.'.COL_IDKATEGORI, $filterKategori);
    }
    if(!empty($filterPartai)) {
      $this->db->where("(select IdPartai from tkandidatpartai where (tkandidatpartai.IdKandidat = tkandidat.Uniq and tkandidatpartai.IdPartai = $filterPartai) limit 1) = $filterPartai");
    }

    $this->db->order_by(COL_IDKATEGORI, 'asc');
    if(!empty($_POST['order'])){
      $order = $orderables[$_POST['order']['0']['column']];
      $this->db->order_by($order, $_POST['order']['0']['dir']);
    }else {
      if(!empty($filterKategori) && $filterKategori != 1 && $filterKategori != 3) {
        $this->db->order_by('(select mpartai.ParNoUrut from tkandidatpartai left join mpartai on mpartai.Uniq = tkandidatpartai.IdPartai where tkandidatpartai.IdKandidat = tkandidat.Uniq limit 1)', 'asc');
        $this->db->order_by(COL_IDDAPIL, 'asc');
      }
      $this->db->order_by(COL_KANDNO, 'asc');
    }

    $q = $this->db
    ->select("tkandidat.*, mdapil.Dapil")
    ->join(TBL_MDAPIL,TBL_MDAPIL.'.'.COL_UNIQ." = ".TBL_TKANDIDAT.".".COL_IDDAPIL,"left")
    ->where(TBL_TKANDIDAT.'.'.COL_ISDELETED, 0)
    ->get_compiled_select(TBL_TKANDIDAT, FALSE);

    //echo $q;
    //exit();
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start")->result_array();
    $recFiltered = $this->db->query($q)->num_rows();
    $recTotal = $queryAll->num_rows();
    $data = [];

    $this->db->reset_query();
    foreach($rec as $r) {
      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/pemilu/calon-edit/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-primary btn-edit"><i class="fas fa-edit"></i></a>&nbsp;';
      $htmlBtn .= '<a href="'.site_url('site/pemilu/calon-delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-danger btn-action"><i class="fas fa-times-circle"></i></a>';

      $arrPartai = array();
      $rpartai = $this->db
      ->join(TBL_MPARTAI,TBL_MPARTAI.'.'.COL_UNIQ." = ".TBL_TKANDIDATPARTAI.".".COL_IDPARTAI,"inner")
      ->where(COL_IDKANDIDAT, $r[COL_UNIQ])
      ->get(TBL_TKANDIDATPARTAI)
      ->result_array();
      foreach($rpartai as $d) {
        $arrPartai[]=$d[COL_PARID];
      }
      $data[] = array(
        $htmlBtn,
        $r[COL_KANDNO],
        $r[COL_KANDNAMA].(!empty($r[COL_KANDNAMAWAKIL])?' - '.$r[COL_KANDNAMAWAKIL]:''),

        !empty($r[COL_DAPIL])?$r[COL_DAPIL]:'-',
        implode(", ",$arrPartai)
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $recFiltered,
      "recordsTotal" => $recTotal,
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function calon_add($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }
    $rpemilu = $this->db->where(COL_UNIQ, $id)->get(TBL_TPEMILU)->row_array();
    if(empty($rpemilu)) {
      echo 'Parameter tidak valid.';
      exit();
    }

    if(!empty($_POST)) {
      $arrPartai = $this->input->post('ArrPartai');
      $det = array();

      $nmfile = null;
      if (!empty($_FILES['userfile']['name'])) {
        $config['upload_path'] = MY_UPLOADPATH.'calon/';
        $config['allowed_types'] = "jpg|jpeg|png";
        $config['max_size']	= 10240;
        $config['overwrite'] = FALSE;

        $this->load->library('upload',$config);
        $res = $this->upload->do_upload('userfile');
        if(!$res) {
          $err = $this->upload->display_errors('', '');
          ShowJsonError($err);
          exit();
        }
        $upl = $this->upload->data();
        $nmfile = $upl['file_name'];
      }

      $dat = array(
        COL_IDPEMILU=>$id,
        COL_IDKATEGORI=>$this->input->post(COL_IDKATEGORI),
        COL_IDDAPIL=>$this->input->post(COL_IDDAPIL),
        COL_KANDNAMA=>$this->input->post(COL_KANDNAMA),
        COL_KANDNAMAWAKIL=>$this->input->post(COL_KANDNAMAWAKIL),
        COL_KANDNO=>$this->input->post(COL_KANDNO),
        COL_KANDFOTO=>$nmfile,
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TKANDIDAT, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }

        $idCalon = $this->db->insert_id();
        if(!empty($arrPartai)) {
          foreach ($arrPartai as $d) {
            $det[] = array(COL_IDKANDIDAT=>$idCalon, COL_IDPARTAI=>$d);
          }

          $res = $this->db->insert_batch(TBL_TKANDIDATPARTAI, $det);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        }
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }

      $this->db->trans_commit();
      ShowJsonSuccess('Data berhasil ditambahkan.', array('redirect'=>site_url('site/pemilu/calon/'.$id)));
      exit();
    } else {
      $this->load->view('pemilu/calon-form');
    }
  }

  public function calon_edit($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      show_error('Maaf, anda tidak memiliki hak akses.');
      exit();
    }
    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_TKANDIDAT)->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    if(!empty($_POST)) {
      $arrPartai = $this->input->post('ArrPartai');
      $det = array();

      $nmfile = $rdata[COL_KANDFOTO];
      if (!empty($_FILES['userfile']['name'])) {
        $config['upload_path'] = MY_UPLOADPATH.'calon/';
        $config['allowed_types'] = "jpg|jpeg|png";
        $config['max_size']	= 10240;
        $config['overwrite'] = FALSE;

        $this->load->library('upload',$config);
        $res = $this->upload->do_upload('userfile');
        if(!$res) {
          $err = $this->upload->display_errors('', '');
          ShowJsonError($err);
          exit();
        }
        $upl = $this->upload->data();
        $nmfile = $upl['file_name'];
      }

      $dat = array(
        COL_IDKATEGORI=>$this->input->post(COL_IDKATEGORI),
        COL_IDDAPIL=>$this->input->post(COL_IDDAPIL),
        COL_KANDNAMA=>$this->input->post(COL_KANDNAMA),
        COL_KANDNAMAWAKIL=>$this->input->post(COL_KANDNAMAWAKIL),
        COL_KANDNO=>$this->input->post(COL_KANDNO),
        COL_KANDFOTO=>$nmfile,
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TKANDIDAT, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $res = $this->db->where(COL_IDKANDIDAT, $id)->delete(TBL_TKANDIDATPARTAI);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        if(!empty($arrPartai)) {
          foreach ($arrPartai as $d) {
            $det[] = array(COL_IDKANDIDAT=>$id, COL_IDPARTAI=>$d);
          }

          $res = $this->db->insert_batch(TBL_TKANDIDATPARTAI, $det);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil memperbarui data.');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['data'] = $rdata;
      $this->load->view('pemilu/calon-form', $data);
    }
  }

  public function calon_delete($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      ShowJsonError('Maaf, anda tidak memiliki hak akses.');
      exit();
    }
    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_TKANDIDAT)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TKANDIDAT, array(COL_ISDELETED=>1));
    if(!$res) {
      ShowJsonError('Gagal menghapus data.');
      exit();
    }

    ShowJsonSuccess('Berhasil menghapus data.');
    exit();
  }
}
