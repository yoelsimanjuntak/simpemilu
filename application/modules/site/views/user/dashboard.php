<?php
$_users = $this->db->count_all_results(TBL__USERS);
$_kecamatan = $this->db->where(COL_ISDELETED,0)->count_all_results(TBL_MKECAMATAN);
$_kelurahan = $this->db->where(COL_ISDELETED,0)->count_all_results(TBL_MKELURAHAN);
$_parpol = $this->db->where(COL_ISDELETED,0)->count_all_results(TBL_MPARTAI);

$rpemilu = $this->db
->where(COL_ISDELETED, 0)
->order_by(COL_TANGGAL,'desc')
->get(TBL_TPEMILU)
->row_array();

$rpemiludet = array();
$arrdet = array();
$rpemilucalon = 0;
$rtps = 0;
if(!empty($rpemilu)) {
  $rpemilucalon = $this->db
  ->where(COL_IDPEMILU, $rpemilu[COL_UNIQ])
  ->count_all_results(TBL_TKANDIDAT);

  $rpemiludet = $this->db
  ->join(TBL_MKATEGORI,TBL_MKATEGORI.'.'.COL_UNIQ." = ".TBL_TPEMILUDET.".".COL_IDKATEGORI,"inner")
  ->where(COL_IDPEMILU, $rpemilu[COL_UNIQ])
  ->get(TBL_TPEMILUDET)
  ->result_array();
  foreach($rpemiludet as $d) {
    $arrdet[]=$d[COL_KATNAMA];
  }

  $rtps = $this->db
  ->where(COL_IDPEMILU, $rpemilu[COL_UNIQ])
  ->count_all_results(TBL_TPEMILUTPS);
}

$_aduanMasuk = $this->db->where(COL_LAPORSTATUS,'DITERIMA')->count_all_results(TBL_TLAPOR);
$_aduanProses = $this->db->where(COL_LAPORSTATUS,'PROSES')->count_all_results(TBL_TLAPOR);
$_aduanSelesai = $this->db->where(COL_LAPORSTATUS,'SELESAI')->count_all_results(TBL_TLAPOR);
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-3 col-6">
        <div class="small-box bg-info">
          <div class="inner">
            <h3><?=number_format($_users)?></h3>
            <p>Pengguna</p>
          </div>
          <div class="icon">
            <i class="fas fa-users"></i>
          </div>
          <a href="<?=site_url('site/user/index')?>" class="small-box-footer">SELENGKAPNYA&nbsp;<i class="far fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-3 col-6">
        <div class="small-box bg-info">
          <div class="inner">
            <h3><?=number_format($_kecamatan)?></h3>
            <p>Kecamatan</p>
          </div>
          <div class="icon">
            <i class="fas fa-map"></i>
          </div>
          <a href="<?=site_url('site/master/kecamatan')?>" class="small-box-footer">SELENGKAPNYA&nbsp;<i class="far fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-3 col-6">
        <div class="small-box bg-info">
          <div class="inner">
            <h3><?=number_format($_kelurahan)?></h3>
            <p>Kelurahan</p>
          </div>
          <div class="icon">
            <i class="fas fa-map-signs"></i>
          </div>
          <a href="<?=site_url('site/master/kelurahan')?>" class="small-box-footer">SELENGKAPNYA&nbsp;<i class="far fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-3 col-6">
        <div class="small-box bg-info">
          <div class="inner">
            <h3><?=number_format($_parpol)?></h3>
            <p>Partai Politik</p>
          </div>
          <div class="icon">
            <i class="fas fa-flag"></i>
          </div>
          <a href="<?=site_url('site/master/kecamatan')?>" class="small-box-footer">SELENGKAPNYA&nbsp;<i class="far fa-arrow-circle-right"></i></a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6 col-6">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title"><i class="fas fa-box-ballot"></i>&nbsp;&nbsp;PEMILU SERENTAK 2024</h3>
            <div class="card-tools">
              <a href="<?=site_url('site/pemilu/detail')?>" class="btn btn-tool btn-sm">
                <i class="fas fa-search"></i>
              </a>
            </div>
          </div>
          <div class="card-body p-0">
            <table class="table table-striped table-valign-middle">
              <tbody>
                <tr>
                  <td style="width: 100px; white-space: nowrap">Jadwal</td>
                  <td style="width: 10px">:</td>
                  <td class="font-weight-bold"><?=!empty($rpemilu)?date('d-m-Y', strtotime($rpemilu[COL_TANGGAL])):'-'?></td>
                </tr>
                <tr>
                  <td style="width: 100px; white-space: nowrap">Pemilihan</td>
                  <td style="width: 10px">:</td>
                  <td class="font-weight-bold"><?=!empty($rpemilu)?(!empty($arrdet)?implode($arrdet,', '):'-'):'-'?></td>
                </tr>
                <tr>
                  <td style="width: 100px; white-space: nowrap">Jumlah TPS</td>
                  <td style="width: 10px">:</td>
                  <td class="font-weight-bold"><?=!empty($rpemilu)?number_format($rtps):'-'?></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-lg-6 col-6">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title"><i class="fas fa-comments-alt"></i>&nbsp;&nbsp;ADUAN</h3>
          </div>
          <div class="card-body p-0">
            <table class="table table-striped table-valign-middle">
              <tbody>
                <tr>
                  <td style="width: 100px; white-space: nowrap">Masuk</td>
                  <td style="width: 10px">:</td>
                  <td class="font-weight-bold"><?=number_format($_aduanMasuk)?></td>
                </tr>
                <tr>
                  <td style="width: 100px; white-space: nowrap">Diproses</td>
                  <td style="width: 10px">:</td>
                  <td class="font-weight-bold"><?=number_format($_aduanProses)?></td>
                </tr>
                <tr>
                  <td style="width: 100px; white-space: nowrap">Selesai</td>
                  <td style="width: 10px">:</td>
                  <td class="font-weight-bold"><?=number_format($_aduanSelesai)?></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
