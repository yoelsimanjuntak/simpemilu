<form id="form-main" method="post" enctype="multipart/form-data" action="<?=current_url()?>">
  <div class="row">
    <div class="col-sm-12">
      <div class="form-group">
        <label>Kabupaten</label>
        <select class="form-control" name="<?=COL_IDKABUPATEN?>" style="width: 100%">
          <?=GetCombobox("select * from mkabupaten order by Kabupaten", COL_UNIQ, COL_KABUPATEN, (!empty($data)?$data[COL_IDKABUPATEN]:null))?>
        </select>
      </div>
      <div class="form-group">
        <label>Kecamatan</label>
        <input type="text" class="form-control" name="<?=COL_KECAMATAN?>" value="<?=!empty($data)?$data[COL_KECAMATAN]:''?>" placeholder="Nama Kecamatan" required />
      </div>
      <div class="form-group">
        <label>Dapil</label>
        <select class="form-control" name="<?=COL_DAPIL?>" style="width: 100%">
          <?=GetCombobox("select * from mdapil order by Dapil", COL_UNIQ, COL_DAPIL, (!empty($data)?$data[COL_DAPIL]:null))?>
        </select>
      </div>
    </div>
  </div>
</form>
