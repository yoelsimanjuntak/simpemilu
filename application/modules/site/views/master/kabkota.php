<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6 text-right">
        <p class="mb-0">
          <?=anchor('site/master/kabkota-add','<i class="far fa-plus-circle"></i> TAMBAH DATA',array('class'=>'btn btn-primary btn-sm btn-add'))?>
        </p>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <div class="card-body p-0">
            <table class="table table-striped projects">
              <thead>
                <tr>
                  <th style="width: 10px; white-space: nowrap" class="text-right">No</th>
                  <th>Kabupaten / Kota</th>
                  <th style="width: 10px; white-space: nowrap; text-align: center">#</th>
                </tr>
              </thead>
              <tbody>
                <?php
                if(empty($res)) {
                  ?>
                  <tr>
                    <td colspan="3" class="font-italic text-center">Belum ada data tersedia.</td>
                  </tr>
                  <?php
                }
                $no=1;
                foreach($res as $r) {
                  ?>
                  <tr>
                    <td style="width: 10px; white-space: nowrap" class="text-right"><?=$no?></td>
                    <td><?=$r[COL_KABUPATEN]?></td>
                    <td style="width: 10px; white-space: nowrap">
                      <a href="<?=site_url('site/master/kabkota-edit/'.$r[COL_UNIQ])?>" class="btn btn-outline-primary btn-sm btn-edit"><i class="far fa-search"></i></a>&nbsp;
                      <a href="<?=site_url('site/master/kabkota-delete/'.$r[COL_UNIQ])?>" class="btn btn-outline-danger btn-sm btn-delete"><i class="far fa-times-circle"></i></a>
                    </td>
                  </tr>
                  <?php
                  $no++;
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-form" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">Form Kabupaten / Kota</span>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
        <button type="submit" class="btn btn-sm btn-primary"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  $('button[type=submit]', $('#modal-form')).click(function(){
    var dis = $(this);
    dis.html("Loading...").attr("disabled", true);
    $('form', $('#modal-form')).ajaxSubmit({
      dataType: 'json',
      success : function(data){
        if(data.error==0) {
          location.reload();
        } else {
          toastr.error(data.error);
        }
      },
      complete: function(data) {
        dis.html('<i class="far fa-check-circle"></i>&nbsp;SIMPAN').attr("disabled", false);
      }
    });
  });
  $('.btn-add, .btn-edit').click(function() {
    var href = $(this).attr('href');
    var modal = $('#modal-form');

    $('.modal-body', modal).html('<p class="text-center">MEMUAT...</p>');
    modal.modal('show');
    $('.modal-body', modal).load(href, function(){

    });
    return false;
  });
  $('.btn-delete').click(function() {
    var url = $(this).attr('href');
    if(confirm('Apakah anda yakin?')) {
      $.get(url, function(res) {
        if(res.error != 0) {
          toastr.error(res.error);
        } else {
          toastr.success(res.success);
          location.reload();
        }
      }, "json").done(function() {

      }).fail(function() {
        toastr.error('Maaf, telah terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi.');
      });
    }
    return false;
  });
});
</script>
