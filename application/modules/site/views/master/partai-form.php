<form id="form-main" method="post" enctype="multipart/form-data" action="<?=current_url()?>">
  <div class="row">
    <div class="col-sm-2">
      <div class="form-group">
        <label>No. Urut</label>
        <input type="text" class="form-control" name="<?=COL_PARNOURUT?>" value="<?=!empty($data)?$data[COL_PARNOURUT]:''?>" required />
      </div>
    </div>
    <div class="col-sm-3">
      <div class="form-group">
        <label>Kode</label>
        <input type="text" class="form-control" name="<?=COL_PARID?>" value="<?=!empty($data)?$data[COL_PARID]:''?>" required />
      </div>
    </div>
    <div class="col-sm-7">
      <div class="form-group">
        <label>Nama Partai Politik</label>
        <input type="text" class="form-control" name="<?=COL_PARNAMA?>" value="<?=!empty($data)?$data[COL_PARNAMA]:''?>" required />
      </div>
    </div>
  </div>
  <div class="row mt-3">
    <div class="col-sm-4 d-flex justify-content-center align-items-center">
      <div style="height: 5rem; width: 5rem; background-image: url('<?=!empty($data)&&!empty($data[COL_PARLOGO])&&file_exists(MY_UPLOADPATH.'partai/'.$data[COL_PARLOGO])?MY_UPLOADURL.'partai/'.$data[COL_PARLOGO]:MY_IMAGEURL.'img-no-image.png'?>'); background-size: cover; border-radius: 10%"></div>
    </div>
    <div class="col-sm-8">
      <label>Logo</label>
      <div class="input-group">
        <div class="custom-file">
          <input type="file" class="custom-file-input" name="userfile" accept="image/*">
          <label class="custom-file-label" for="userfile">Unggah Gambar</label>
        </div>
      </div>
      <small class="text-muted font-italic">Ukuran maks. 2MB</small>
    </div>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function() {
  bsCustomFileInput.init();
});
</script>
