<?php
$rkabkota = $this->db
->query('select * from mkabupaten order by Kabupaten asc')
->result_array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <div class="card-header">
            <div class="card-tools text-center" style="float: none !important">
              <a href="<?=site_url('site/master/kelurahan-add')?>" type="button" class="btn btn-tool btn-add text-primary"><i class="fas fa-plus"></i>&nbsp;TAMBAH DATA</a>
              <button type="button" class="btn btn-tool btn-refresh-data"><i class="fas fa-sync-alt"></i>&nbsp;REFRESH</button>
            </div>
          </div>
          <div class="card-body">
            <form id="dataform" method="post" action="#">
              <table id="datalist" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 10px">AKSI</th>
                    <th>KABUPATEN / KOTA</th>
                    <th>KECAMATAN</th>
                    <th>KELURAHAN</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div id="dom-filter-kabkota" class="d-none">
  <select class="form-control" name="filterKabkota" style="width: 300px">
    <option value="">- SEMUA KABUPATEN / KOTA -</option>
    <?php
    foreach($rkabkota as $kab) {
      ?>
      <option value="<?=$kab[COL_UNIQ]?>" data-url="<?=site_url('site/master/get-opt-kecamatan/'.$kab[COL_UNIQ])?>"><?=$kab[COL_KABUPATEN]?></option>
      <?php
    }
    ?>
  </select>
</div>
<div id="dom-filter-kecamatan" class="d-none">
  <select class="form-control" name="filterKecamatan" style="width: 300px">
    <option value="">- SEMUA KECAMATAN -</option>
  </select>
</div>
<div class="modal fade" id="modal-form" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">Form Kelurahan</span>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
        <button type="submit" class="btn btn-sm btn-primary"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  var dt = $('#datalist').dataTable({
    "autoWidth" : false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?=site_url('site/master/kelurahan-load')?>",
      "type": 'POST',
      "data": function(data){
        data.filterKabkota = $('[name=filterKabkota]', $('.filtering1')).val();
        data.filterKecamatan = $('[name=filterKecamatan]', $('.filtering2')).val();
       }
    },
    "scrollY" : '40vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    "oLanguage": {
      "sSearch": "FILTER "
    },
    //"aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    //"dom":"R<'row'<'col-sm-8 filtering'><'col-sm-4 text-right'f<'clear'>B>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    "dom":"R<'row'<'col-sm-12 d-flex'f<'filtering1'><'filtering2'>>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    "order": [],
    "columnDefs": [
      {"targets":[0], "className":'nowrap text-center'},
      {"targets":[1,2,3], "className":'nowrap'}
    ],
    "columns": [
      {"orderable": false,"width": "50px"},
      {"orderable": true},
      {"orderable": true},
      {"orderable": true}
    ],
    "createdRow": function(row, data, dataIndex) {
      $('.btn-action', $(row)).click(function() {
        var url = $(this).attr('href');
        if(confirm('Apakah anda yakin?')) {
          $.get(url, function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success(res.success);
            }
          }, "json").done(function() {
            dt.DataTable().ajax.reload();
          }).fail(function() {
            toastr.error('SERVER ERROR');
          });
        }
        return false;
      });
      $('.btn-edit', $(row)).click(function() {
        var href = $(this).attr('href');
        var modal = $('#modal-form');

        $('.modal-body', modal).html('<p class="text-center">MEMUAT...</p>');
        modal.modal('show');
        $('.modal-body', modal).load(href, function(){
          $("select", modal).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
        });
        return false;
      });
    },
    "initComplete": function(settings, json) {
      $('input[type=search]', $('#datalist_filter')).removeClass('form-control-sm').attr('placeholder', 'Keyword');
    }
  });
  $("div.filtering1").html($('#dom-filter-kabkota').html()).addClass('d-inline-block ml-2');
  $("div.filtering2").html($('#dom-filter-kecamatan').html()).addClass('d-inline-block ml-2');

  $('.btn-refresh-data').click(function() {
    dt.DataTable().ajax.reload();
  });
  $('input,select', $("div.filtering1")).change(function() {
    dt.DataTable().ajax.reload();
  });
  $('input,select', $("div.filtering2")).change(function() {
    dt.DataTable().ajax.reload();
  });

  $('.btn-add').click(function() {
    var href = $(this).attr('href');
    var modal = $('#modal-form');

    $('.modal-body', modal).html('<p class="text-center">MEMUAT...</p>');
    modal.modal('show');
    $('.modal-body', modal).load(href, function(){
      $("select", modal).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
    });
    return false;
  });

  $('button[type=submit]', $('#modal-form')).click(function(){
    var dis = $(this);
    var modal = $('#modal-form');
    dis.html("Loading...").attr("disabled", true);
    $('form', modal).ajaxSubmit({
      dataType: 'json',
      success : function(data){
        dt.DataTable().ajax.reload();
        if(data.error==0) {

        } else {
          toastr.error(data.error);
        }
      },
      complete: function(data) {
        modal.modal('hide');
        dis.html('<i class="far fa-check-circle"></i>&nbsp;SIMPAN').attr("disabled", false);
      }
    });
  });

  $('[name=filterKabkota]').change(function(){
    var url = $('option:selected', $('[name=filterKabkota]')).data('url');
    if(url) {
      $('[name=filterKecamatan]').load(url, function(){
        $('select[name=filterKecamatan]').select2({ width: 'resolve', theme: 'bootstrap4' });
      });
    } else {
      $('[name=filterKecamatan]').html('<option value="">- SEMUA KECAMATAN -</option>');
      $('select[name=filterKecamatan]').select2({ width: 'resolve', theme: 'bootstrap4' });
    }
  });
});
</script>
