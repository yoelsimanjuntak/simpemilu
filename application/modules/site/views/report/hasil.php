<?php
$rkecamatan = $this->db
->query('select mkecamatan.*, kab.Kabupaten from mkecamatan left join mkabupaten kab on kab.Uniq = mkecamatan.IdKabupaten order by Kabupaten asc, Kecamatan asc')
->result_array();

$rkategori = $this->db
->query('select * from mkategori order by Uniq asc')
->result_array();

$_kat = $this->input->get('kat');
$_kec = $this->input->get('kec');
$_kel = $this->input->get('kel');
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-outline card-primary">
          <div class="card-header">
            <h5 class="card-title">Filter Hasil</h5>
          </div>
          <form id="form-filter" method="get" action="<?=current_url()?>">
            <div class="card-body pb-0">
              <div class="row">
                <div class="col-sm-3">
                  <div class="form-group">
                    <label>Pemilihan Umum</label>
                    <select class="form-control select2" name="filterPemilu" style="width: 100%">
                      <?=GetCombobox("select * from tpemilu where IsDeleted=0", COL_UNIQ, COL_JUDUL, (!empty($rpemilu)?$rpemilu[COL_UNIQ]:null))?>
                    </select>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <label>Kategori</label>
                    <select class="form-control select2" name="filterKategori" style="width: 100%">
                      <?php
                      foreach($rkategori as $kat) {
                        ?>
                        <option value="<?=$kat[COL_UNIQ]?>" data-kategori="<?=$kat[COL_KATNAMA]?>" <?=!empty($_kat)&&$_kat==$kat[COL_UNIQ]?'selected':(!empty($data)&&$data[COL_IDKATEGORI]==$kat[COL_UNIQ]?'selected':'')?>><?=$kat[COL_KATDESC]?></option>
                        <?php
                      }
                      ?>
                    </select>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <label>Kecamatan</label>
                    <select class="form-control" name="filterKecamatan" style="width: 100%">
                      <option value="">- SEMUA KECAMATAN -</option>
                      <?php
                      foreach($rkecamatan as $kec) {
                        ?>
                        <option value="<?=$kec[COL_UNIQ]?>" <?=!empty($data)&&$data[COL_IDKECAMATAN]==$kec[COL_UNIQ]?'selected':''?> data-url="<?=site_url('site/master/get-opt-kelurahan/'.$kec[COL_UNIQ])?>"><?=$kec[COL_KABUPATEN].' - '.$kec[COL_KECAMATAN]?></option>
                        <?php
                      }
                      ?>
                    </select>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <label>Kelurahan</label>
                    <select class="form-control" name="filterKelurahan" style="width: 100%" required>
                      <option value="">- SEMUA KELURAHAN -</option>
                      <!--<option value="">- PILIH KELURAHAN -</option>-->
                    </select>
                  </div>
                </div>
              </div>
              <div class="row p-3 mb-0" style="border-top: 1px solid #dedede; margin-left: -20px; margin-right: -20px;">
                <div class="col-sm-12 pr-0 text-right">
                  <a href="<?=site_url('site/home/live-view')?>" class="btn btn-danger"><i class="far fa-eye"></i>&nbsp; LIVE VIEW</a>
                  <a href="<?=site_url('site/report/hasil')?>" class="btn btn-success btn-filter">TAMPILKAN &nbsp;<i class="far fa-arrow-circle-right"></i></a>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div class="row" id="hasil">
      <div class="col-sm-12 d-none">
        <p class="text-center"><i class="far fa-spinner-third fa-spin"></i>&nbsp; MEMUAT DATA ...</p>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function() {
  $('[name=filterKecamatan]').change(function(){
    var url = $('option:selected', $('[name=filterKecamatan]')).data('url');
    if(url) {
      $('[name=filterKelurahan]').load(url, function(){
        $('select[name=filterKelurahan]').select2({ width: 'resolve', theme: 'bootstrap4' });
        $('select[name=filterKelurahan]').trigger('change');
      });
    } else {
      $('[name=filterKelurahan]').html('<option value="">- SEMUA KELURAHAN -</option>');
      $('select[name=filterKelurahan]').select2({ width: 'resolve', theme: 'bootstrap4' });
    }
  }).trigger('change');

  $('.btn-filter', $('form#form-filter')).click(function(){
    var _url = $(this).attr('href');
    var _idPemilu = $('option:selected', $('[name=filterPemilu]', $('form#form-filter'))).val();
    var _idKategori = $('option:selected', $('[name=filterKategori]', $('form#form-filter'))).val();
    var _idKecamatan = $('option:selected', $('[name=filterKecamatan]', $('form#form-filter'))).val();
    var _idKelurahan = $('option:selected', $('[name=filterKelurahan]', $('form#form-filter'))).val();

    $('#hasil').html('<div class="col-sm-12"><p class="text-center"><i class="far fa-spinner-third fa-spin"></i>&nbsp; MEMUAT DATA ...</p></div>');
    if(_idPemilu && _idKategori) {
      $('#hasil').load(_url, {idPemilu: _idPemilu, idKategori: _idKategori, idKecamatan: _idKecamatan, idKelurahan: _idKelurahan}, function(){

      });
    } else {
      $('#hasil').html('<div class="col-sm-12"><p class="text-center">PARAMETER TIDAK VALID ...!</p></div>');
    }

    return false;
  });
});
</script>
