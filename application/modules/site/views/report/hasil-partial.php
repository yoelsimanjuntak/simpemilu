<div class="col-sm-12">
  <div class="card card-outline card-primary">
    <div class="card-header">
      <h3 class="card-title">Hasil Perhitungan Suara</h3>
    </div>
    <div class="card-body p-0">
      <?php
      if($rkategori[COL_KATNAMA]=='PPWP' || $rkategori[COL_KATNAMA]=='DPD') {
        $rcalon = $this->db
        ->where(COL_IDPEMILU, $idPemilu)
        ->where(COL_IDKATEGORI, $idKategori)
        ->order_by(COL_KANDNO, 'asc')
        ->get(TBL_TKANDIDAT)
        ->result_array();
        ?>
        <table class="table">
          <thead>
            <tr>
              <th>No.</th>
              <th>Nama</th>
              <th>Jlh. Suara</th>
            </tr>
          </thead>
          <tbody>
            <?php
            foreach($rcalon as $cal) {
              if(!empty($idKecamatan)) {
                $this->db->where(COL_IDKECAMATAN, $idKecamatan);
                if(!empty($idKelurahan)) {
                  $this->db->where(COL_IDKELURAHAN, $idKelurahan);
                }
              }
              $rsum = $this->db
              ->select('sum(thasildet.JlhSuara) as JlhSuara')
              ->join(TBL_THASIL,TBL_THASIL.'.'.COL_UNIQ." = ".TBL_THASILDET.".".COL_IDHASIL,"left")
              ->join(TBL_TPEMILUTPS,TBL_TPEMILUTPS.'.'.COL_UNIQ." = ".TBL_THASIL.".".COL_IDTPS,"left")
              ->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_UNIQ." = ".TBL_TPEMILUTPS.".".COL_IDKELURAHAN,"left")
              ->where(TBL_THASILDET.'.'.COL_IDKANDIDAT, $cal[COL_UNIQ])
              ->where(TBL_THASIL.'.'.COL_IDPEMILU, $idPemilu)
              ->where(TBL_THASIL.'.'.COL_IDKATEGORI, $idKategori)
              ->get(TBL_THASILDET)
              ->row_array();
              ?>
              <tr>
                <td class="text-right" style="width: 10px; white-space: nowrap"><?=$cal[COL_KANDNO]?></td>
                <td><?=$cal[COL_KANDNAMA]?><?=!empty($cal[COL_KANDNAMAWAKIL])?' & '.$cal[COL_KANDNAMAWAKIL]:''?></td>
                <td class="text-center"><?=!empty($rsum)?number_format($rsum[COL_JLHSUARA]):'-'?></td>
              </tr>
              <?php
            }
            ?>
          </tbody>
        </table>
        <?php
      } else {
        if($rkategori[COL_KATNAMA]=='DPRD-KAB-KOTA' && !empty($idKecamatan)) {
          $this->db->where(COL_IDDAPIL, $rkecamatan[COL_DAPIL]);
        }
        $rpartai = $this->db
        ->join(TBL_TKANDIDATPARTAI,TBL_TKANDIDATPARTAI.'.'.COL_IDKANDIDAT." = ".TBL_TKANDIDAT.".".COL_UNIQ,"inner")
        ->join(TBL_MPARTAI,TBL_MPARTAI.'.'.COL_UNIQ." = ".TBL_TKANDIDATPARTAI.".".COL_IDPARTAI,"inner")
        ->where(COL_IDPEMILU, $idPemilu)
        ->where(COL_IDKATEGORI, $idKategori)
        ->group_by(COL_IDPARTAI)
        ->order_by(COL_PARNOURUT, 'asc')
        ->get(TBL_TKANDIDAT)
        ->result_array();
        ?>
        <table class="table mb-0">
          <thead>
            <tr>
              <th>No.</th>
              <th>Nama</th>
              <th>Jlh. Suara</th>
            </tr>
          </thead>
          <tbody>
            <?php
            foreach($rpartai as $par) {
              if(!empty($idKecamatan)) {
                $this->db->where(COL_IDKECAMATAN, $idKecamatan);
                if(!empty($idKelurahan)) {
                  $this->db->where(COL_IDKELURAHAN, $idKelurahan);
                }
              }
              $rsum = $this->db
              ->select('sum(thasildet.JlhSuara) as JlhSuara')
              ->join(TBL_THASIL,TBL_THASIL.'.'.COL_UNIQ." = ".TBL_THASILDET.".".COL_IDHASIL,"left")
              ->join(TBL_TPEMILUTPS,TBL_TPEMILUTPS.'.'.COL_UNIQ." = ".TBL_THASIL.".".COL_IDTPS,"left")
              ->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_UNIQ." = ".TBL_TPEMILUTPS.".".COL_IDKELURAHAN,"left")
              ->where(TBL_THASILDET.'.'.COL_IDPARTAI, $par[COL_IDPARTAI])
              ->where(TBL_THASIL.'.'.COL_IDPEMILU, $idPemilu)
              ->where(TBL_THASIL.'.'.COL_IDKATEGORI, $idKategori)
              ->get(TBL_THASILDET)
              ->row_array();

              if($rkategori[COL_KATNAMA]=='DPRD-KAB-KOTA' && !empty($idKecamatan)) {
                $this->db->where(COL_IDDAPIL, $rkecamatan[COL_DAPIL]);
              }
              $rcalon = $this->db
              ->select('tkandidat.*')
              ->join(TBL_TKANDIDATPARTAI,TBL_TKANDIDATPARTAI.'.'.COL_IDKANDIDAT." = ".TBL_TKANDIDAT.".".COL_UNIQ,"inner")
              ->join(TBL_MPARTAI,TBL_MPARTAI.'.'.COL_UNIQ." = ".TBL_TKANDIDATPARTAI.".".COL_IDPARTAI,"inner")
              ->where(COL_IDPEMILU, $idPemilu)
              ->where(COL_IDKATEGORI, $idKategori)
              ->where(COL_IDPARTAI, $par[COL_IDPARTAI])
              ->order_by(COL_KANDNO, 'asc')
              ->get(TBL_TKANDIDAT)
              ->result_array();
              ?>
              <tr class="bg-light">
                <td class="font-weight-bold text-right" style="width: 10px; white-space: nowrap"><?=$par[COL_PARNOURUT]?></td>
                <td class="font-weight-bold"><?=strtoupper($par[COL_PARNAMA])?></td>
                <td class="text-center" class="font-weight-bold">
                  <?=!empty($rsum)?number_format($rsum[COL_JLHSUARA]):'-'?>
                </td>
              </tr>
              <?php
              foreach($rcalon as $cal) {
                if(!empty($idKecamatan)) {
                  $this->db->where(COL_IDKECAMATAN, $idKecamatan);
                  if(!empty($idKelurahan)) {
                    $this->db->where(COL_IDKELURAHAN, $idKelurahan);
                  }
                }
                $rsum = $this->db
                ->select('sum(thasildet.JlhSuara) as JlhSuara')
                ->join(TBL_THASIL,TBL_THASIL.'.'.COL_UNIQ." = ".TBL_THASILDET.".".COL_IDHASIL,"left")
                ->join(TBL_TPEMILUTPS,TBL_TPEMILUTPS.'.'.COL_UNIQ." = ".TBL_THASIL.".".COL_IDTPS,"left")
                ->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_UNIQ." = ".TBL_TPEMILUTPS.".".COL_IDKELURAHAN,"left")
                ->where(TBL_THASILDET.'.'.COL_IDKANDIDAT, $cal[COL_UNIQ])
                ->where(TBL_THASIL.'.'.COL_IDPEMILU, $idPemilu)
                ->where(TBL_THASIL.'.'.COL_IDKATEGORI, $idKategori)
                ->get(TBL_THASILDET)
                ->row_array();
                ?>
                <tr>
                  <td class="text-right" style="width: 10px; white-space: nowrap"><?=$cal[COL_KANDNO]?></td>
                  <td><?=$cal[COL_KANDNAMA]?><?=!empty($cal[COL_KANDNAMAWAKIL])?' & '.$cal[COL_KANDNAMAWAKIL]:''?></td>
                  <td class="text-center" class="font-weight-bold">
                    <?=!empty($rsum)?number_format($rsum[COL_JLHSUARA]):'-'?>
                  </td>
                </tr>
                <?php
              }
            }
            ?>
          </tbody>
        </table>
        <?php
      }
      ?>
    </div>
    <div class="card-footer p-0">
      <ul class="nav nav-pills flex-column">
        <li class="nav-item">
          <a href="#" class="nav-link p-3 text-dark">JLH. SUARA SAH<span class="float-right font-weight-bold"><?=number_format($rhasil['Jlh_Suara_Sah'])?></span></a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link p-3 text-dark">PROGRESS PERHITUNGAN (TPS)<span class="float-right"><strong><?=number_format($rhasil['Jlh_TPS'])?></strong> / <?=number_format($numTPS)?></span></a>
        </li>
      </ul>
    </div>
  </div>
</div>
