<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <?php
      foreach($res as $r) {
        $arrdet = array();
        $rdet = $this->db
        ->select('tpemiludet.*, mkategori.KatNama, mkategori.KatDesc, (select sum(Jlh_Suara_Sah) from thasil where thasil.IdPemilu=tpemiludet.IdPemilu and thasil.IdKategori=tpemiludet.IdKategori) as Jlh_Suara_Sah, (select count(IdTPS) from thasil where thasil.IdPemilu=tpemiludet.IdPemilu and thasil.IdKategori=tpemiludet.IdKategori) as Jlh_TPS_Hasil')
        ->join(TBL_MKATEGORI,TBL_MKATEGORI.'.'.COL_UNIQ." = ".TBL_TPEMILUDET.".".COL_IDKATEGORI,"inner")
        ->where(COL_IDPEMILU, $r[COL_UNIQ])
        ->get(TBL_TPEMILUDET)
        ->result_array();
        $rtps = $this->db
        ->where(COL_IDPEMILU, $r[COL_UNIQ])
        ->count_all_results(TBL_TPEMILUTPS);
        $rkandidat = $this->db
        ->where(COL_IDPEMILU, $r[COL_UNIQ])
        ->count_all_results(TBL_TKANDIDAT);
        foreach($rdet as $d) {
          $arrdet[]=$d[COL_KATNAMA];
        }
        ?>
        <div class="col-sm-6">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title"><?=$r[COL_JUDUL]?></h3>
            </div>
            <div class="card-body p-0">
              <table class="table table-striped table-condensed table-valign-middle">
                <tbody>
                  <tr>
                    <td style="width: 100px; white-space: nowrap">Jadwal</td>
                    <td style="width: 10px">:</td>
                    <td class="font-weight-bold"><?=date('d-m-Y', strtotime($r[COL_TANGGAL]))?></td>
                  </tr>
                  <tr>
                    <td style="width: 100px; white-space: nowrap">Waktu Perhitungan</td>
                    <td style="width: 10px">:</td>
                    <td><strong><?=date('H:i', strtotime($r[COL_HITUNGMULAI]))?></strong> s.d <strong><?=date('H:i', strtotime($r[COL_HITUNGAKHIR]))?></strong></td>
                  </tr>
                  <tr>
                    <td style="width: 100px; white-space: nowrap">Jumlah TPS</td>
                    <td style="width: 10px">:</td>
                    <td class="font-weight-bold"><?=number_format($rtps)?></td>
                  </tr>
                </tbody>
              </table>

              <table class="table table-bordered table-condensed table-valign-middle" style="border-top: 1px solid #dedede">
                <thead>
                  <tr>
                    <th>Pemilihan</th>
                    <th style="width: 100px; white-space: nowrap">Jlh. Suara Sah</th>
                    <th style="width: 200px; white-space: nowrap">Progress</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  foreach($rdet as $d) {
                    $perc = 0;
                    $bg = 'danger';
                    if($rtps>0) {
                      $perc = $d['Jlh_TPS_Hasil']/$rtps*100;
                    }
                    if($perc>=25) $bg = 'warning';
                    else if($perc>=50) $bg = 'success';
                    else if($perc>=100) $bg = 'primary';
                    ?>
                    <tr>
                      <td><?=$d[COL_KATDESC]?></th>
                      <td style="width: 100px; white-space: nowrap" class="text-right"><?=number_format($d['Jlh_Suara_Sah'])?></td>
                      <td style="width: 100px; white-space: nowrap">
                        <div class="progress-group">
                          <span class="text-sm"><?=number_format($perc,2)?>%</span>
                          <span class="text-sm float-right"><b><?=number_format($d['Jlh_TPS_Hasil'])?></b> / <?=number_format($rtps)?></span>
                          <div class="progress progress-sm">
                            <div class="progress-bar progress-bar-striped bg-<?=$bg?>" style="width: <?=number_format($perc,2)?>%"></div>
                          </div>
                        </div>
                      </td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
            <div class="card-footer text-right">
              <a href="<?=site_url('site/result/detail/'.$r[COL_UNIQ])?>" class="btn btn-info btn-sm <?=strtotime(date('Y-m-d H:i:s'))<strtotime($r[COL_HITUNGMULAI])||strtotime(date('Y-m-d H:i:s'))>strtotime($r[COL_HITUNGAKHIR])?'':''?>"><i class="fas fa-box-ballot"></i>&nbsp; KELOLA HASIL PERHITUNGAN</a>
            </div>
          </div>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</section>
