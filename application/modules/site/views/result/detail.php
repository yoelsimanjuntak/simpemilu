<?php
$rkecamatan = $this->db
->query('select mkecamatan.*, kab.Kabupaten from mkecamatan left join mkabupaten kab on kab.Uniq = mkecamatan.IdKabupaten order by Kabupaten asc, Kecamatan asc')
->result_array();

$rkategori = $this->db
->query('select * from mkategori order by Uniq asc')
->result_array();

$_kat = $this->input->get('kat');
$_kec = $this->input->get('kec');
$_kel = $this->input->get('kel');
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url('site/result/index')?>">HASIL PERHITUNGAN</a></li>
          <li class="breadcrumb-item active"><?=$rpemilu[COL_JUDUL]?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-outline card-primary">
          <div class="card-header">
            <h5 class="card-title">LOKASI PERHITUNGAN</h5>
          </div>
          <form id="form-filter" method="get" action="<?=current_url()?>">
            <div class="card-body">
              <div class="row">
                <div class="col-sm-3">
                  <div class="form-group">
                    <label>Kategori</label>
                    <select class="form-control select2" name="filterKategori" style="width: 100%">
                      <?php
                      foreach($rkategori as $kat) {
                        ?>
                        <option value="<?=$kat[COL_UNIQ]?>" data-kategori="<?=$kat[COL_KATNAMA]?>" <?=!empty($_kat)&&$_kat==$kat[COL_UNIQ]?'selected':(!empty($data)&&$data[COL_IDKATEGORI]==$kat[COL_UNIQ]?'selected':'')?>><?=$kat[COL_KATDESC]?></option>
                        <?php
                      }
                      ?>
                    </select>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <label>Kecamatan</label>
                    <select class="form-control" name="filterKecamatan" style="width: 100%" required>
                      <?php
                      foreach($rkecamatan as $kec) {
                        ?>
                        <option value="<?=$kec[COL_UNIQ]?>" <?=!empty($data)&&$data[COL_IDKECAMATAN]==$kec[COL_UNIQ]?'selected':''?> data-url="<?=site_url('site/master/get-opt-kelurahan/'.$kec[COL_UNIQ].'/noempty')?>"><?=$kec[COL_KABUPATEN].' - '.$kec[COL_KECAMATAN]?></option>
                        <?php
                      }
                      ?>
                    </select>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <label>Kelurahan</label>
                    <select class="form-control" name="filterKelurahan" style="width: 100%" required>
                      <!--<option value="">- PILIH KELURAHAN -</option>-->
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div class="row" id="tps">
      <div class="col-sm-12">
        <p class="text-center"><i class="far fa-spinner-third fa-spin"></i>&nbsp; MEMUAT DATA ...</p>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function() {
  $('select', $('form#form-filter')).change(function(){
    var _idKategori = $('option:selected', $('[name=filterKategori]', $('form#form-filter'))).val();
    var _idKecamatan = $('option:selected', $('[name=filterKecamatan]', $('form#form-filter'))).val();
    var _idKelurahan = $('option:selected', $('[name=filterKelurahan]', $('form#form-filter'))).val();

    $('#tps').html('<div class="col-sm-12"><p class="text-center"><i class="far fa-spinner-third fa-spin"></i>&nbsp; MEMUAT DATA ...</p></div>');
    if(_idKategori && _idKecamatan && _idKelurahan) {
      $('#tps').load('<?=site_url('site/result/detail-partial/'.$rpemilu[COL_UNIQ])?>', {idKategori: _idKategori, idKecamatan: _idKecamatan, idKelurahan: _idKelurahan}, function(){

      });
    }
  });

  <?php
  if(!empty($_kec)) {
    ?>
    $('option[value="<?=$_kec?>"]', $('[name=filterKecamatan]')).prop('selected', true);
    <?php
  }
  ?>
  $('[name=filterKecamatan]').change(function(){
    var url = $('option:selected', $('[name=filterKecamatan]')).data('url');
    if(url) {
      $('[name=filterKelurahan]').load(url, function(){
        $('select[name=filterKelurahan]').select2({ width: 'resolve', theme: 'bootstrap4' });
        <?php
        if(!empty($_kec)) {
          ?>
          $('option[value="<?=$_kel?>"]', $('[name=filterKelurahan]')).prop('selected', true);
          <?php
        }
        ?>
        $('select[name=filterKelurahan]').trigger('change');
      });
    } else {
      //$('[name=IdKelurahan]').html('<option value="">- PILIH KELURAHAN -</option>');
      $('select[name=filterKelurahan]').select2({ width: 'resolve', theme: 'bootstrap4' });
    }
  }).trigger('change');
});
</script>
