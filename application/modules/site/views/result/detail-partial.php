<?php
foreach($rtps as $r) {
  $rhasil = $this->db
  ->where(COL_IDPEMILU, $r[COL_IDPEMILU])
  ->where(COL_IDKATEGORI, $idKategori)
  ->where(COL_IDTPS, $r[COL_UNIQ])
  ->get(TBL_THASIL)
  ->row_array();
  ?>
  <div class="col-sm-4">
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title"><i class="fas fa-box-ballot"></i>&nbsp;TPS <?=str_pad($r[COL_TPSNAMA], 2, '0', STR_PAD_LEFT)?></h3>
      </div>
      <div class="card-body p-0">
        <table class="table table-striped table-valign-middle">
          <tbody>
            <tr>
              <td colspan="3" class="font-italic"><?=$r[COL_TPSKETERANGAN]?></td>
            </tr>
            <tr>
              <td style="width: 100px; white-space: nowrap">Jlh. DPT</td>
              <td style="width: 10px">:</td>
              <td class="font-weight-bold"><?=!empty($rhasil)?number_format($rhasil[COL_JLH_DPT_PRIA]+$rhasil[COL_JLH_DPT_WANITA]):'-'?></td>
            </tr>
            <!--<tr>
              <td style="width: 100px; white-space: nowrap">Jlh. Pengguna Hak Pilih</td>
              <td style="width: 10px">:</td>
              <td class="font-weight-bold"><?=!empty($rhasil)?number_format($rhasil[COL_JLH_DPTPEMILIH_PRIA]+$rhasil[COL_JLH_DPTPEMILIH_WANITA]+$rhasil[COL_JLH_DPTBPEMILIH_PRIA]+$rhasil[COL_JLH_DPTBPEMILIH_WANITA]+$rhasil[COL_JLH_DPKPEMILIH_PRIA]+$rhasil[COL_JLH_DPKPEMILIH_WANITA]):'-'?></td>
            </tr>-->
            <tr>
              <td style="width: 100px; white-space: nowrap">Jlh. Suara Sah</td>
              <td style="width: 10px">:</td>
              <td class="font-weight-bold"><?=!empty($rhasil)?number_format($rhasil[COL_JLH_SUARA_SAH]):'-'?></td>
            </tr>
            <tr>
              <td style="width: 100px; white-space: nowrap">Jlh. Suara Tidak Sah</td>
              <td style="width: 10px">:</td>
              <td class="font-weight-bold"><?=!empty($rhasil)?number_format($rhasil[COL_JLH_SUARA_TIDAKSAH]):'-'?></td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="card-footer text-right">
        <a href="<?=site_url('site/result/form/'.$idKategori.'/'.$r[COL_UNIQ])?>" class="btn btn-info btn-sm"><i class="fas fa-edit"></i>&nbsp; KELOLA DATA HASIL</a>
      </div>
    </div>
  </div>
  <?php
}
?>
