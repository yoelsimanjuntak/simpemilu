<form id="form-main" class="form-horizontal pt-1" method="post" action="<?=current_url()?>?tab=total">
  <h6 style="margin-left: -20px; margin-right: -20px;" class="font-weight-bold bg-info p-3 mb-4">SUARA SAH DAN TIDAK SAH</h6>
  <div class="form-group row">
    <label class="control-label col-sm-3 pr-5">JUMLAH TOTAL SUARA SAH</label>
    <div class="col-sm-2">
      <input type="number" class="form-control text-right" name="<?=COL_JLH_SUARA_SAH?>" value="<?=!empty($rdata)?$rdata[COL_JLH_SUARA_SAH]:''?>" required />
    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-3 pr-5">JUMLAH TOTAL SUARA TIDAK SAH</label>
    <div class="col-sm-2">
      <input type="number" class="form-control text-right" name="<?=COL_JLH_SUARA_TIDAKSAH?>" value="<?=!empty($rdata)?$rdata[COL_JLH_SUARA_TIDAKSAH]:''?>" required />
    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-3 pr-5">TOTAL</label>
    <div class="col-sm-2">
      <input type="number" class="form-control text-right" name="Total" value="<?=!empty($rdata)?$rdata[COL_JLH_SUARA_SAH]+$rdata[COL_JLH_SUARA_TIDAKSAH]:''?>" readonly />
    </div>
  </div>

  <div class="form-group row p-3 mb-0" style="border-top: 1px solid #dedede; margin-left: -20px; margin-right: -20px;">
    <div class="col-sm-12 text-right">
      <button type="submit" class="btn btn-primary" data-next-url="<?=current_url()?>">SIMPAN DAN LANJUT &nbsp;<i class="far fa-arrow-circle-right"></i></button>
    </div>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function() {
  $('button[type=submit]', $('#form-main')).click(function(){
    var dis = $(this);
    dis.html("Loading...").attr("disabled", true);
    $('#form-main').ajaxSubmit({
      dataType: 'json',
      success : function(data){
        if(data.error==0) {
          if(data.redirect) location.href = data.redirect;
          else location.reload();
        } else {
          toastr.error(data.error);
        }
      },
      complete: function(data) {
        dis.html('SIMPAN DAN LANJUT &nbsp;<i class="far fa-arrow-circle-right"></i>').attr("disabled", false);
      }
    });
  });

  $('[name=Jlh_Suara_Sah],[name=Jlh_Suara_TidakSah]').change(function(){
    var jlhSah = ($('[name=Jlh_Suara_Sah]').val() || 0);
    var jlhTidakSah = ($('[name=Jlh_Suara_TidakSah]').val() || 0);

    $('[name=Total]').val(parseInt(jlhSah)+parseInt(jlhTidakSah));
  });
});
</script>
