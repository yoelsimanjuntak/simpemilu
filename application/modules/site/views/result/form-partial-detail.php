<?php
$rkelurahan = $this->db
->select('mkelurahan.*, mkecamatan.Dapil')
->join(TBL_MKECAMATAN,TBL_MKECAMATAN.'.'.COL_UNIQ." = ".TBL_MKELURAHAN.".".COL_IDKECAMATAN,"inner")
->where(TBL_MKELURAHAN.'.'.COL_UNIQ, $rtps[COL_IDKELURAHAN])
->get(TBL_MKELURAHAN)
->row_array();
?>
<form id="form-main" class="form-horizontal" method="post" action="<?=current_url()?>?tab=detail">
  <input type="hidden" value="" name="ArrHasil" />
  <div class="row pt-2" style="margin-left: -20px; margin-right: -20px;">
    <div class="col-sm-12 p-0">
      <?php
      if($rkategori[COL_KATNAMA]=='PPWP' || $rkategori[COL_KATNAMA]=='DPD') {
        $rcalon = $this->db
        ->where(COL_IDPEMILU, $rtps[COL_IDPEMILU])
        ->where(COL_IDKATEGORI, $rkategori[COL_UNIQ])
        ->order_by(COL_KANDNO, 'asc')
        ->get(TBL_TKANDIDAT)
        ->result_array();
        ?>
        <table class="table">
          <thead>
            <tr>
              <th style="width: 10px; white-space: nowrap">No. Urut</th>
              <th>Nama</th>
              <th style="width: 200px; white-space: nowrap">Jlh. Suara Sah</th>
            </tr>
          </thead>
          <tbody>
            <?php
            foreach($rcalon as $cal) {
              $rexist = $this->db
              ->where(COL_IDHASIL, $rdata[COL_UNIQ])
              ->where(COL_IDKANDIDAT, $cal[COL_UNIQ])
              ->get(TBL_THASILDET)
              ->row_array();
              ?>
              <tr>
                <td class="text-right" style="width: 10px; white-space: nowrap"><?=$cal[COL_KANDNO]?></td>
                <td><?=$cal[COL_KANDNAMA]?><?=!empty($cal[COL_KANDNAMAWAKIL])?' & '.$cal[COL_KANDNAMAWAKIL]:''?></td>
                <td style="width: 200px; white-space: nowrap">
                  <input type="number" class="form-control text-right input-hasil" data-idpartai="" data-idcalon="<?=$cal[COL_UNIQ]?>" value="<?=!empty($rexist)?$rexist[COL_JLHSUARA]:''?>" required />
                </td>
              </tr>
              <?php
            }
            ?>
          </tbody>
        </table>
        <?php
      } else {
        if($rkategori[COL_KATNAMA]=='DPRD-KAB-KOTA') {
          $this->db->where(COL_IDDAPIL, $rkelurahan[COL_DAPIL]);
        }
        $rpartai = $this->db
        ->join(TBL_TKANDIDATPARTAI,TBL_TKANDIDATPARTAI.'.'.COL_IDKANDIDAT." = ".TBL_TKANDIDAT.".".COL_UNIQ,"inner")
        ->join(TBL_MPARTAI,TBL_MPARTAI.'.'.COL_UNIQ." = ".TBL_TKANDIDATPARTAI.".".COL_IDPARTAI,"inner")
        ->where(COL_IDPEMILU, $rtps[COL_IDPEMILU])
        ->where(COL_IDKATEGORI, $rkategori[COL_UNIQ])
        ->group_by(COL_IDPARTAI)
        ->order_by(COL_PARNOURUT, 'asc')
        ->get(TBL_TKANDIDAT)
        ->result_array();
        ?>
        <table class="table mb-0">
          <thead>
            <tr>
              <th style="width: 10px; white-space: nowrap">No. Urut</th>
              <th>Nama</th>
              <th style="width: 200px; white-space: nowrap">Jlh. Suara Sah</th>
            </tr>
          </thead>
          <tbody>
            <?php
            foreach($rpartai as $par) {
              $rexist = $this->db
              ->where(COL_IDHASIL, $rdata[COL_UNIQ])
              ->where(COL_IDPARTAI, $par[COL_UNIQ])
              ->get(TBL_THASILDET)
              ->row_array();

              if($rkategori[COL_KATNAMA]=='DPRD-KAB-KOTA') {
                $this->db->where(COL_IDDAPIL, $rkelurahan[COL_DAPIL]);
              }
              $rcalon = $this->db
              ->select('tkandidat.*')
              ->join(TBL_TKANDIDATPARTAI,TBL_TKANDIDATPARTAI.'.'.COL_IDKANDIDAT." = ".TBL_TKANDIDAT.".".COL_UNIQ,"inner")
              ->join(TBL_MPARTAI,TBL_MPARTAI.'.'.COL_UNIQ." = ".TBL_TKANDIDATPARTAI.".".COL_IDPARTAI,"inner")
              ->where(COL_IDPEMILU, $rtps[COL_IDPEMILU])
              ->where(COL_IDKATEGORI, $rkategori[COL_UNIQ])
              ->where(COL_IDPARTAI, $par[COL_IDPARTAI])
              ->order_by(COL_KANDNO, 'asc')
              ->get(TBL_TKANDIDAT)
              ->result_array();
              ?>
              <tr class="bg-light">
                <td class="font-weight-bold text-right" style="width: 10px; white-space: nowrap"><?=$par[COL_PARNOURUT]?></td>
                <td class="font-weight-bold"><?=strtoupper($par[COL_PARNAMA])?></td>
                <td class="font-weight-bold" style="width: 200px; white-space: nowrap">
                  <input type="number" class="form-control text-right input-hasil" data-idpartai="<?=$par[COL_UNIQ]?>" data-idcalon="" value="<?=!empty($rexist)?$rexist[COL_JLHSUARA]:''?>" required />
                </td>
              </tr>
              <?php
              foreach($rcalon as $cal) {
                $rexist = $this->db
                ->where(COL_IDHASIL, $rdata[COL_UNIQ])
                ->where(COL_IDKANDIDAT, $cal[COL_UNIQ])
                ->get(TBL_THASILDET)
                ->row_array();
                ?>
                <tr>
                  <td class="text-right" style="width: 10px; white-space: nowrap"><?=$cal[COL_KANDNO]?></td>
                  <td class="pl-5"><?=$cal[COL_KANDNAMA]?><?=!empty($cal[COL_KANDNAMAWAKIL])?' & '.$cal[COL_KANDNAMAWAKIL]:''?></td>
                  <td style="width: 200px; white-space: nowrap">
                    <input type="number" class="form-control text-right input-hasil" data-idpartai="" data-idcalon="<?=$cal[COL_UNIQ]?>" value="<?=!empty($rexist)?$rexist[COL_JLHSUARA]:''?>" required />
                  </td>
                </tr>
                <?php
              }
            }
            ?>
          </tbody>
        </table>
        <?php
      }
      ?>
      <div class="form-group p-3 mb-0 text-right" style="border-top: 1px solid #dedede;">
        <button type="submit" class="btn btn-primary" data-next-url="<?=current_url()?>">SIMPAN DAN LANJUT &nbsp;<i class="far fa-arrow-circle-right"></i></button>
      </div>
    </div>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function() {
  $('button[type=submit]', $('#form-main')).click(function(){
    var arrhasil = [];
    var elDetail = $('input.input-hasil', $('#form-main'));
    for(var i=0; i<elDetail.length; i++) {
      var idPartai = $(elDetail[i]).data('idpartai');
      var idCalon = $(elDetail[i]).data('idcalon');
      var numSuara = parseInt($(elDetail[i]).val());

      arrhasil.push({IdPartai: idPartai, IdKandidat: idCalon, JlhSuara: numSuara});
    }
    $('[name=ArrHasil]', $('#form-main')).val(JSON.stringify(arrhasil));

    var dis = $(this);
    dis.html("Loading...").attr("disabled", true);
    $('#form-main').ajaxSubmit({
      dataType: 'json',
      success : function(data){
        if(data.error==0) {
          if(data.redirect) location.href = data.redirect;
          else location.reload();
        } else {
          toastr.error(data.error);
        }
      },
      complete: function(data) {
        dis.html('SIMPAN DAN LANJUT &nbsp;<i class="far fa-arrow-circle-right"></i>').attr("disabled", false);
      }
    });
  });
});
</script>
