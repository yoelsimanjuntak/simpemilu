<form id="form-main" method="post" enctype="multipart/form-data" action="<?=current_url()?>">
  <div class="form-group">
    <label>Nama Dokumen</label>
    <input type="text" class="form-control" name="<?=COL_DOCNAME?>" required />
  </div>
  <div class="form-group">
    <label>File</label>
    <div class="input-group">
      <div class="custom-file">
        <input type="file" class="custom-file-input" name="userfile" accept="image/*,application/pdf">
        <label class="custom-file-label" for="userfile">Unggah File / Dokumen</label>
      </div>
    </div>
    <small class="text-muted font-italic">Ukuran maks. 5MB</small>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function() {
  bsCustomFileInput.init();
});
</script>
