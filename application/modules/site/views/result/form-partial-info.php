<form id="form-main" class="form-horizontal pt-1" method="post" action="<?=current_url()?>?tab=info">
  <h6 style="margin-left: -20px; margin-right: -20px;" class="font-weight-bold bg-info p-3 mb-4">DATA PEMILIH DAN PENGGUNA HAK PILIH</h6>
  <div class="form-group row">
    <label class="control-label col-sm-3 pt-2 mt-3 pr-5">JUMLAH DPT</label>
    <div class="col-sm-2">
      <label>Pria</label>
      <input type="number" class="form-control text-right" name="<?=COL_JLH_DPT_PRIA?>" value="<?=!empty($rdata)?$rdata[COL_JLH_DPT_PRIA]:''?>" required />
    </div>
    <div class="col-sm-2">
      <label>Wanita</label>
      <input type="number" class="form-control text-right" name="<?=COL_JLH_DPT_WANITA?>" value="<?=!empty($rdata)?$rdata[COL_JLH_DPT_WANITA]:''?>" required />
    </div>
    <div class="col-sm-2">
      <label>Total</label>
      <input type="number" class="form-control text-right" name="Total_DPT" value="<?=!empty($rdata)?$rdata[COL_JLH_DPT_PRIA]+$rdata[COL_JLH_DPT_WANITA]:''?>" readonly />
    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-3 pt-2 mt-3 pr-5">JUMLAH PENGGUNA HAK PILIH (DPT)</label>
    <div class="col-sm-2">
      <label>Pria</label>
      <input type="number" class="form-control text-right" name="<?=COL_JLH_DPTPEMILIH_PRIA?>" value="<?=!empty($rdata)?$rdata[COL_JLH_DPTPEMILIH_PRIA]:''?>" required />
    </div>
    <div class="col-sm-2">
      <label>Wanita</label>
      <input type="number" class="form-control text-right" name="<?=COL_JLH_DPTPEMILIH_WANITA?>" value="<?=!empty($rdata)?$rdata[COL_JLH_DPTPEMILIH_WANITA]:''?>" required />
    </div>
    <div class="col-sm-2">
      <label>Total</label>
      <input type="number" class="form-control text-right" name="Total_DPTPemilih" value="<?=!empty($rdata)?$rdata[COL_JLH_DPTPEMILIH_PRIA]+$rdata[COL_JLH_DPTPEMILIH_WANITA]:''?>" readonly />
    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-3 pt-2 mt-3 pr-5">JUMLAH PENGGUNA HAK PILIH (DPTb)</label>
    <div class="col-sm-2">
      <label>Pria</label>
      <input type="number" class="form-control text-right" name="<?=COL_JLH_DPTBPEMILIH_PRIA?>" value="<?=!empty($rdata)?$rdata[COL_JLH_DPTBPEMILIH_PRIA]:''?>" required />
    </div>
    <div class="col-sm-2">
      <label>Wanita</label>
      <input type="number" class="form-control text-right" name="<?=COL_JLH_DPTBPEMILIH_WANITA?>" value="<?=!empty($rdata)?$rdata[COL_JLH_DPTBPEMILIH_WANITA]:''?>" required />
    </div>
    <div class="col-sm-2">
      <label>Total</label>
      <input type="number" class="form-control text-right" name="Total_DPTbPemilih" value="<?=!empty($rdata)?$rdata[COL_JLH_DPTBPEMILIH_PRIA]+$rdata[COL_JLH_DPTBPEMILIH_WANITA]:''?>" readonly />
    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-3 pt-2 mt-3 pr-5">JUMLAH PENGGUNA HAK PILIH (DPK)</label>
    <div class="col-sm-2">
      <label>Pria</label>
      <input type="number" class="form-control text-right" name="<?=COL_JLH_DPKPEMILIH_PRIA?>" value="<?=!empty($rdata)?$rdata[COL_JLH_DPKPEMILIH_PRIA]:''?>" required />
    </div>
    <div class="col-sm-2">
      <label>Wanita</label>
      <input type="number" class="form-control text-right" name="<?=COL_JLH_DPKPEMILIH_WANITA?>" value="<?=!empty($rdata)?$rdata[COL_JLH_DPKPEMILIH_WANITA]:''?>" required />
    </div>
    <div class="col-sm-2">
      <label>Total</label>
      <input type="number" class="form-control text-right" name="Total_DPKPemilih" value="<?=!empty($rdata)?$rdata[COL_JLH_DPKPEMILIH_PRIA]+$rdata[COL_JLH_DPKPEMILIH_WANITA]:''?>" readonly />
    </div>
  </div>

  <h6 style="margin-left: -20px; margin-right: -20px;" class="font-weight-bold bg-info p-3 mb-4">DATA PENGGUNA SURAT SUARA</h6>
  <div class="form-group row">
    <label class="control-label col-sm-3 pr-5">JUMLAH SURAT SUARA DITERIMA</label>
    <div class="col-sm-2">
      <input type="number" class="form-control text-right" name="<?=COL_JLH_SS_DITERIMA?>" value="<?=!empty($rdata)?$rdata[COL_JLH_SS_DITERIMA]:''?>" required />
    </div>
    <div class="col-sm-4 pt-2">
      <p class="text-sm font-italic">Termasuk surat suara cadangan 2% dari DPT</p>
    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-3 pr-5">JUMLAH SURAT SUARA DIGUNAKAN</label>
    <div class="col-sm-2">
      <input type="number" class="form-control text-right" name="<?=COL_JLH_SS_DIGUNAKAN?>" value="<?=!empty($rdata)?$rdata[COL_JLH_SS_DIGUNAKAN]:''?>" required />
    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-3 pr-5">JUMLAH SURAT SUARA DIKEMBALIKAN</label>
    <div class="col-sm-2">
      <input type="number" class="form-control text-right" name="<?=COL_JLH_SS_DIKEMBALIKAN?>" value="<?=!empty($rdata)?$rdata[COL_JLH_SS_DIKEMBALIKAN]:''?>" required />
    </div>
    <div class="col-sm-4 pt-2">
      <p class="text-sm font-italic">Dikembalikan pemilih karena rusak atau keliru coblos</p>
    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-3 pr-5">JUMLAH SURAT SUARA SISA</label>
    <div class="col-sm-2">
      <input type="number" class="form-control text-right" name="<?=COL_JLH_SS_SISA?>" value="<?=!empty($rdata)?$rdata[COL_JLH_SS_SISA]:''?>" required />
    </div>
    <div class="col-sm-4 pt-2">
      <p class="text-sm font-italic">Tidak digunakan / tidak terpakai, termasuk surat suara cadangan</p>
    </div>
  </div>

  <h6 style="margin-left: -20px; margin-right: -20px;" class="font-weight-bold bg-info p-3 mb-4">DATA PEMILIH DISABILITAS</h6>
  <div class="form-group row">
    <label class="control-label col-sm-3 pt-2 mt-3 pr-5">JUMLAH PENGGUNA HAK PILIH DISABILITAS</label>
    <div class="col-sm-2">
      <label>Pria</label>
      <input type="number" class="form-control text-right" name="<?=COL_JLH_DISABLITAS_PRIA?>" value="<?=!empty($rdata)?$rdata[COL_JLH_DISABLITAS_PRIA]:''?>" required />
    </div>
    <div class="col-sm-2">
      <label>Wanita</label>
      <input type="number" class="form-control text-right" name="<?=COL_JLH_DISABILITAS_WANITA?>" value="<?=!empty($rdata)?$rdata[COL_JLH_DISABILITAS_WANITA]:''?>" required />
    </div>
    <div class="col-sm-2">
      <label>Total</label>
      <input type="number" class="form-control text-right" name="Total_Disabilitas" value="<?=!empty($rdata)?$rdata[COL_JLH_DISABLITAS_PRIA]+$rdata[COL_JLH_DISABILITAS_WANITA]:''?>" readonly />
    </div>
  </div>

  <div class="form-group row p-3 mb-0" style="border-top: 1px solid #dedede; margin-left: -20px; margin-right: -20px;">
    <div class="col-sm-12 text-right">
      <button type="submit" class="btn btn-primary" data-next-url="<?=current_url()?>">SIMPAN DAN LANJUT &nbsp;<i class="far fa-arrow-circle-right"></i></button>
    </div>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function() {
  $('button[type=submit]', $('#form-main')).click(function(){
    var dis = $(this);
    dis.html("Loading...").attr("disabled", true);
    $('#form-main').ajaxSubmit({
      dataType: 'json',
      success : function(data){
        if(data.error==0) {
          if(data.redirect) location.href = data.redirect;
          else location.reload();
        } else {
          toastr.error(data.error);
        }
      },
      complete: function(data) {
        dis.html('SIMPAN DAN LANJUT &nbsp;<i class="far fa-arrow-circle-right"></i>').attr("disabled", false);
      }
    });
  });

  $('[name=Jlh_DPT_Pria],[name=Jlh_DPT_Wanita]').change(function(){
    var jlhPria = ($('[name=Jlh_DPT_Pria]').val() || 0);
    var jlhWanita = ($('[name=Jlh_DPT_Wanita]').val() || 0);

    $('[name=Total_DPT]').val(parseInt(jlhPria)+parseInt(jlhWanita));
  });
  $('[name=Jlh_DPTPemilih_Pria],[name=Jlh_DPTPemilih_Wanita]').change(function(){
    var jlhPria = ($('[name=Jlh_DPTPemilih_Pria]').val() || 0);
    var jlhWanita = ($('[name=Jlh_DPTPemilih_Wanita]').val() || 0);

    $('[name=Total_DPTPemilih]').val(parseInt(jlhPria)+parseInt(jlhWanita));
  });
  $('[name=Jlh_DPTBPemilih_Pria],[name=Jlh_DPTBPemilih_Wanita]').change(function(){
    var jlhPria = ($('[name=Jlh_DPTBPemilih_Pria]').val() || 0);
    var jlhWanita = ($('[name=Jlh_DPTBPemilih_Wanita]').val() || 0);

    $('[name=Total_DPTbPemilih]').val(parseInt(jlhPria)+parseInt(jlhWanita));
  });
  $('[name=Jlh_DPKPemilih_Pria],[name=Jlh_DPKPemilih_Wanita]').change(function(){
    var jlhPria = ($('[name=Jlh_DPKPemilih_Pria]').val() || 0);
    var jlhWanita = ($('[name=Jlh_DPKPemilih_Wanita]').val() || 0);

    $('[name=Total_DPKPemilih]').val(parseInt(jlhPria)+parseInt(jlhWanita));
  });
  $('[name=Jlh_Disablitas_Pria],[name=Jlh_Disabilitas_Wanita]').change(function(){
    var jlhPria = ($('[name=Jlh_Disablitas_Pria]').val() || 0);
    var jlhWanita = ($('[name=Jlh_Disabilitas_Wanita]').val() || 0);

    $('[name=Total_Disabilitas]').val(parseInt(jlhPria)+parseInt(jlhWanita));
  });
});
</script>
