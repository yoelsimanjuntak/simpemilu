<?php
$rpemilu = $this->db
->where(COL_UNIQ, $rtps[COL_IDPEMILU])
->get(TBL_TPEMILU)
->row_array();

$q = $this->input->get('tab');
$data['rkategori'] = $rkategori;
$data['rtps'] = $rtps;
$data['rdata'] = $rdata;
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url('site/result/index')?>">HASIL PERHITUNGAN</a></li>
          <li class="breadcrumb-item"><a href="<?=site_url('site/result/detail/'.$rpemilu[COL_UNIQ]).'?kat='.$rkategori[COL_UNIQ].'&kec='.$rtps[COL_IDKECAMATAN].'&kel='.$rtps[COL_IDKELURAHAN]?>"><?=$rpemilu[COL_JUDUL]?></a></li>
          <li class="breadcrumb-item active"><?='TPS '.str_pad($rtps[COL_TPSNAMA],2,"0",STR_PAD_LEFT)?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header d-flex p-0 border-0">
            <h3 class="card-title p-3 font-weight-bold"><?='TPS '.str_pad($rtps[COL_TPSNAMA],2,"0",STR_PAD_LEFT)?> - <?=$rkategori[COL_KATDESC]?></h3>
            <ul class="nav nav-pills ml-auto p-2">
              <li class="nav-item"><a class="nav-link <?=$q=='info'||empty($q)?'active':''?>" href="<?=current_url().'?tab=info'?>">1. DATA PEMILIH</a></li>
              <li class="nav-item"><a class="nav-link <?=$q=='detail'?'active':''?> <?=empty($rdata)?'disabled':''?>" href="<?=current_url().'?tab=detail'?>">2. RINCIAN</a></li>
              <li class="nav-item"><a class="nav-link <?=$q=='total'?'active':''?> <?=empty($rdata)?'disabled':''?>" href="<?=current_url().'?tab=total'?>">3. TOTAL</a></li>
              <li class="nav-item"><a class="nav-link <?=$q=='doc'?'active':''?> <?=empty($rdata)?'disabled':''?>" href="<?=current_url().'?tab=doc'?>">4. LAMPIRAN</a></li>
            </ul>
          </div>
          <div class="card-body pt-0 pb-0">
            <?php
            if($q=='info' || empty($q)) $this->load->view('result/form-partial-info', $data);
            else if($q=='detail') $this->load->view('result/form-partial-detail', $data);
            else if($q=='total') $this->load->view('result/form-partial-total', $data);
            else if($q=='doc') $this->load->view('result/form-partial-doc', $data);
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
