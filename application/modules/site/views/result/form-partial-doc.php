<?php
$rdoc = $this->db
->where(COL_IDHASIL, $rdata[COL_UNIQ])
->where(COL_ISDELETED, 0)
->get(TBL_THASILDOC)
->result_array();
?>
<h6 style="margin-left: -20px; margin-right: -20px;" class="font-weight-bold bg-info p-3 mb-0">DAFTAR LAMPIRAN</h6>
<div class="row" style="margin-left: -20px; margin-right: -20px;">
  <div class="col-sm-12 p-0">
    <table class="table mb-0">
      <thead>
        <tr>
          <th style="width: 10px; white-space: nowrap">No.</th>
          <th>Nama Lampiran</th>
          <th style="width: 100px; white-space: nowrap; text-align: right">Ditambahkan Pada</th>
          <th style="width: 10px; white-space: nowrap">#</th>
        </tr>
      </thead>
      <tbody>
        <?php
        if(!empty($rdoc)) {
          $no=1;
          foreach($rdoc as $doc) {
            ?>
            <tr>
              <td class="text-right" style="width: 10px; white-space: nowrap"><?=$no?></td>
              <td><?=$doc[COL_DOCNAME]?></td>
              <td><?=date('Y-m-d H:i:',strtotime($doc[COL_CREATEDON]))?></td>
              <td class="text-center" style="width: 10px; white-space: nowrap">
                <a href="<?=MY_UPLOADURL.'hasil/'.$doc[COL_DOCURL]?>" class="btn btn-sm btn-outline-success"><i class="far fa-download"></i></a>
                <a href="<?=site_url('site/result/doc-delete/'.$doc[COL_UNIQ])?>" class="btn btn-outline-danger btn-sm btn-delete"><i class="far fa-trash"></i></a>
              </td>
            </tr>
            <?php
            $no++;
          }
        } else {
          ?>
          <tr>
            <td colspan="4" class="text-center font-italic">BELUM ADA DOKUMEN DIUNGGAH</td>
          </tr>
          <?php
        }
        ?>
      </tbody>
    </table>
  </div>
</div>
<div class="row p-3 mb-0" style="border-top: 1px solid #dedede; margin-left: -20px; margin-right: -20px;">
  <div class="col-sm-12 text-right">
    <a href="<?=site_url('site/result/doc-add/'.$rdata[COL_UNIQ])?>" class="btn btn-success btn-add-doc"><i class="fas fa-upload"></i>&nbsp; UNGGAH DOKUMEN</a>
    <a href="<?=site_url('site/result/detail/'.$rdata[COL_IDPEMILU]).'?kat='.$rkategori[COL_UNIQ].'&kec='.$rtps[COL_IDKECAMATAN].'&kel='.$rtps[COL_IDKELURAHAN]?>" class="btn btn-primary"><i class="fas fa-check-circle"></i>&nbsp; SELESAI</a>
  </div>
</div>
<div class="modal fade" id="modal-form" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">Form Lampiran</span>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
        <button type="submit" class="btn btn-sm btn-primary"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  $('button[type=submit]', $('#modal-form')).click(function(){
    var dis = $(this);
    dis.html("Loading...").attr("disabled", true);
    $('form', $('#modal-form')).ajaxSubmit({
      dataType: 'json',
      success : function(data){
        if(data.error==0) {
          location.reload();
        } else {
          toastr.error(data.error);
        }
      },
      complete: function(data) {
        dis.html('<i class="far fa-check-circle"></i>&nbsp;SIMPAN').attr("disabled", false);
      }
    });
  });
  $('.btn-add-doc').click(function() {
    var href = $(this).attr('href');
    var modal = $('#modal-form');

    $('.modal-body', modal).html('<p class="text-center">MEMUAT...</p>');
    modal.modal('show');
    $('.modal-body', modal).load(href, function(){

    });
    return false;
  });
  $('.btn-delete').click(function() {
    var url = $(this).attr('href');
    if(confirm('Apakah anda yakin?')) {
      $.get(url, function(res) {
        if(res.error != 0) {
          toastr.error(res.error);
        } else {
          toastr.success(res.success);
          location.reload();
        }
      }, "json").done(function() {

      }).fail(function() {
        toastr.error('Maaf, telah terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi.');
      });
    }
    return false;
  });
});
</script>
