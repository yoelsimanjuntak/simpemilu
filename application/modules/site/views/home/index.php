<?php
$rpemilu = $this->db
->where(COL_ISDELETED, 0)
->order_by(COL_TANGGAL,'desc')
->get(TBL_TPEMILU)
->row_array();

$_aduanMasuk = $this->db->where(COL_LAPORSTATUS,'DITERIMA')->count_all_results(TBL_TLAPOR);
$_aduanProses = $this->db->where(COL_LAPORSTATUS,'PROSES')->count_all_results(TBL_TLAPOR);
$_aduanSelesai = $this->db->where(COL_LAPORSTATUS,'SELESAI')->count_all_results(TBL_TLAPOR);
?>
<!--<div class="content-header">
  <div class="container">
    <div class="row mb-2">
      <div class="col-12 col-md-6">
        <h5 class="m-0 text-dark font-weight-light">
          <marquee>Selamat datang di <strong><?=$title?><strong></marquee>
        </h5>
      </div>
      <div class="col-12 col-md-6 text-right">
        <button type="button" class="btn btn-sm btn-primary"><i class="fas fa-calendar"></i>&nbsp;<span id="date"></span></button>&nbsp;
        <button type="button" class="btn btn-sm btn-success"><i class="fas fa-clock"></i>&nbsp;<span id="time"></span></button>
      </div>
    </div>
  </div>
</div>-->
<div class="content pt-4">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="card card-outline card-info">
          <div class="card-header">
            <h3 class="card-title"><i class="far fa-box-ballot"></i>&nbsp; <?=!empty($rpemilu)?$rpemilu[COL_JUDUL]:'JADWAL PEMILU'?></h3>
          </div>
          <div class="card-body p-0">
            <?php
            if(!empty($rpemilu)) {
              $arrdet = array();
              $rdet = $this->db
              ->select('tpemiludet.*, mkategori.KatNama, mkategori.KatDesc, (select sum(Jlh_Suara_Sah) from thasil where thasil.IdPemilu=tpemiludet.IdPemilu and thasil.IdKategori=tpemiludet.IdKategori) as Jlh_Suara_Sah, (select count(IdTPS) from thasil where thasil.IdPemilu=tpemiludet.IdPemilu and thasil.IdKategori=tpemiludet.IdKategori) as Jlh_TPS_Hasil')
              ->join(TBL_MKATEGORI,TBL_MKATEGORI.'.'.COL_UNIQ." = ".TBL_TPEMILUDET.".".COL_IDKATEGORI,"inner")
              ->where(COL_IDPEMILU, $rpemilu[COL_UNIQ])
              ->get(TBL_TPEMILUDET)
              ->result_array();

              $rtps = $this->db
              ->where(COL_IDPEMILU, $rpemilu[COL_UNIQ])
              ->count_all_results(TBL_TPEMILUTPS);
              ?>
              <table class="table table-striped table-valign-middle">
                <tbody>
                  <tr>
                    <td style="width: 100px; white-space: nowrap">Jadwal</td>
                    <td style="width: 10px">:</td>
                    <td class="font-weight-bold"><?=date('d-m-Y', strtotime($rpemilu[COL_TANGGAL]))?></td>
                  </tr>
                  <tr>
                    <td style="width: 100px; white-space: nowrap">Waktu Perhitungan</td>
                    <td style="width: 10px">:</td>
                    <td><strong><?=date('H:i', strtotime($rpemilu[COL_HITUNGMULAI]))?></strong> s.d <strong><?=date('H:i', strtotime($rpemilu[COL_HITUNGAKHIR]))?></strong></td>
                  </tr>
                  <tr>
                    <td style="width: 100px; white-space: nowrap">Jumlah TPS</td>
                    <td style="width: 10px">:</td>
                    <td class="font-weight-bold"><?=number_format($rtps)?></td>
                  </tr>
                </tbody>
              </table>
              <div class="table-responsive">
                <table class="table table-condensed table-valign-middle mb-0" style="border-top: 1px solid #dedede">
                  <thead>
                    <tr>
                      <th>Pemilihan</th>
                      <th>Jlh. Suara</th>
                      <th style="width: 200px; white-space: nowrap">Progress</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    foreach($rdet as $d) {
                      $perc = 0;
                      $bg = 'danger';
                      if($rtps>0) {
                        $perc = $d['Jlh_TPS_Hasil']/$rtps*100;
                      }
                      if($perc>=25) $bg = 'warning';
                      else if($perc>=50) $bg = 'success';
                      else if($perc>=100) $bg = 'primary';
                      ?>
                      <tr>
                        <td><?=$d[COL_KATDESC]?></th>
                        <td class="text-right"><?=number_format($d['Jlh_Suara_Sah'])?></td>
                        <td style="width: 100px; white-space: nowrap">
                          <div class="progress-group">
                            <span class="text-sm"><?=number_format($perc,2)?>%</span>
                            <span class="text-sm float-right"><b><?=number_format($d['Jlh_TPS_Hasil'])?></b> / <?=number_format($rtps)?></span>
                            <div class="progress progress-sm">
                              <div class="progress-bar progress-bar-striped bg-<?=$bg?>" style="width: <?=number_format($perc,2)?>%"></div>
                            </div>
                          </div>
                        </td>
                      </tr>
                      <?php
                    }
                    ?>
                  </tbody>
                </table>
              </div>
              <?php
            } else {
              echo '<p class="text-center font-italic mb-0 p-3">Maaf, belum ada data tersedia saat ini.</p>';
            }
            ?>
          </div>
          <div class="card-footer text-right">
            <a href="<?=site_url('site/home/pemilu-detail/'.$rpemilu[COL_UNIQ])?>" class="btn btn-info btn-sm <?=strtotime(date('Y-m-d H:i:s'))<strtotime($rpemilu[COL_HITUNGMULAI])||strtotime(date('Y-m-d H:i:s'))>strtotime($rpemilu[COL_HITUNGAKHIR])?'disabled':''?>"><i class="fas fa-search"></i>&nbsp; LIHAT RINCIAN</a>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="card card-outline card-info">
          <div class="card-header">
            <h3 class="card-title"><i class="far fa-comments-alt"></i>&nbsp; PENGADUAN</h3>
          </div>
          <div class="card-body p-0">
            <table class="table table-striped table-valign-middle">
              <tbody>
                <tr>
                  <td style="width: 100px; white-space: nowrap">Masuk</td>
                  <td style="width: 10px">:</td>
                  <td class="font-weight-bold"><?=number_format($_aduanMasuk)?></td>
                </tr>
                <tr>
                  <td style="width: 100px; white-space: nowrap">Diproses</td>
                  <td style="width: 10px">:</td>
                  <td class="font-weight-bold"><?=number_format($_aduanProses)?></td>
                </tr>
                <tr>
                  <td style="width: 100px; white-space: nowrap">Selesai</td>
                  <td style="width: 10px">:</td>
                  <td class="font-weight-bold"><?=number_format($_aduanSelesai)?></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="card-footer text-right">
            <a href="<?=site_url('site/home/pemilu-lapor')?>" class="btn btn-info btn-sm"><i class="fas fa-plus"></i>&nbsp; BUAT PENGADUAN</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
