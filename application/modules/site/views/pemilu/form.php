<?php
$arrdet = array();
if(!empty($data)) {
  $rdet = $this->db
  ->where(COL_IDPEMILU, $data[COL_UNIQ])
  ->get(TBL_TPEMILUDET)
  ->result_array();
  foreach($rdet as $d) {
    $arrdet[]=$d[COL_IDKATEGORI];
  }
}

?>
<form id="form-main" method="post" enctype="multipart/form-data" action="<?=current_url()?>">
  <div class="row">
    <div class="col-sm-9">
      <div class="form-group">
        <label>Judul</label>
        <input type="text" class="form-control" name="<?=COL_JUDUL?>" value="<?=!empty($data)?$data[COL_JUDUL]:''?>" placeholder="Judul Pemilihan Umum" required />
      </div>
    </div>
    <div class="col-sm-3">
      <div class="form-group">
        <label>Tanggal</label>
        <input type="text" class="form-control datepicker text-right" name="<?=COL_TANGGAL?>" value="<?=!empty($data)?$data[COL_TANGGAL]:''?>" required />
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6">
      <div class="form-group">
        <label>Waktu Pemungutan Suara</label>
        <div class="row">
          <div class="col-sm-6">
            <input type="text" name="<?=COL_PUNGUTMULAI?>" class="form-control text-center time-mask" placeholder="MULAI" name="<?=COL_PUNGUTMULAI?>" value="<?=!empty($data)?date('H:i', strtotime($data[COL_PUNGUTMULAI])):''?>" required />
          </div>
          <div class="col-sm-6">
            <input type="text" name="<?=COL_PUNGUTAKHIR?>" class="form-control text-center time-mask" placeholder="SELESAI" name="<?=COL_PUNGUTAKHIR?>" value="<?=!empty($data)?date('H:i', strtotime($data[COL_PUNGUTAKHIR])):''?>" required />
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6">
      <div class="form-group">
        <label>Waktu Perhitungan Suara</label>
        <div class="row">
          <div class="col-sm-6">
            <input type="text" name="<?=COL_HITUNGMULAI?>" class="form-control text-center time-mask" placeholder="MULAI" name="<?=COL_HITUNGMULAI?>" value="<?=!empty($data)?date('H:i', strtotime($data[COL_HITUNGMULAI])):''?>" required>
          </div>
          <div class="col-sm-6">
            <input type="text" name="<?=COL_HITUNGAKHIR?>" class="form-control text-center time-mask" placeholder="SELESAI" name="<?=COL_HITUNGAKHIR?>" value="<?=!empty($data)?date('H:i', strtotime($data[COL_HITUNGAKHIR])):''?>" required>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="form-group">
        <label>Kategori</label>
        <select class="form-control select2" name="ArrDet[]" style="width: 100%" multiple>
          <?=GetCombobox("select * from mkategori", COL_UNIQ, COL_KATNAMA, (!empty($arrdet)?$arrdet:null))?>
        </select>
      </div>
    </div>
  </div>
</form>
