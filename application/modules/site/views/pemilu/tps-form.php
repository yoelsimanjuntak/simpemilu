<?php
$rkecamatan = $this->db
->query('select mkecamatan.*, kab.Kabupaten from mkecamatan left join mkabupaten kab on kab.Uniq = mkecamatan.IdKabupaten order by Kabupaten asc, Kecamatan asc')
->result_array();
?>
<form id="form-main" method="post" enctype="multipart/form-data" action="<?=current_url()?>">
  <div class="row">
    <div class="col-sm-12">
      <div class="form-group">
        <label>Kecamatan</label>
        <select class="form-control" name="<?=COL_IDKECAMATAN?>" style="width: 100%" required>
          <?php
          foreach($rkecamatan as $kec) {
            ?>
            <option value="<?=$kec[COL_UNIQ]?>" <?=!empty($data)&&$data[COL_IDKECAMATAN]==$kec[COL_UNIQ]?'selected':''?> data-url="<?=site_url('site/master/get-opt-kelurahan/'.$kec[COL_UNIQ].'/noempty')?>"><?=$kec[COL_KABUPATEN].' - '.$kec[COL_KECAMATAN]?></option>
            <?php
          }
          ?>
        </select>
      </div>
      <div class="form-group">
        <label>Kelurahan</label>
        <select class="form-control" name="<?=COL_IDKELURAHAN?>" style="width: 100%" required>
          <option value="">- PILIH KELURAHAN -</option>
        </select>
      </div>
      <div class="form-group">
        <label>Nama TPS</label>
        <input type="text" class="form-control" name="<?=COL_TPSNAMA?>" value="<?=!empty($data)?$data[COL_TPSNAMA]:''?>" placeholder="Nama / Nomor TPS" required />
      </div>
      <div class="form-group">
        <label>Koordinat</label>
        <input type="text" class="form-control" name="<?=COL_TPSKOORDINAT?>" value="<?=!empty($data)?$data[COL_TPSKOORDINAT]:''?>" placeholder="Lat,Long" />
      </div>
      <div class="form-group">
        <label>Keterangan</label>
        <textarea class="form-control" name="<?=COL_TPSKETERANGAN?>"><?=!empty($data)?$data[COL_TPSKETERANGAN]:''?></textarea>
      </div>
    </div>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function() {
  $('[name=IdKecamatan]').change(function(){
    var url = $('option:selected', $('[name=IdKecamatan]')).data('url');
    if(url) {
      $('[name=IdKelurahan]').load(url, function(){
        $('select[name=IdKelurahan]').select2({ width: 'resolve', theme: 'bootstrap4' });
      });
    } else {
      $('[name=IdKelurahan]').html('<option value="">- PILIH KELURAHAN -</option>');
      $('select[name=IdKelurahan]').select2({ width: 'resolve', theme: 'bootstrap4' });
    }
  }).trigger('change');
});
</script>
