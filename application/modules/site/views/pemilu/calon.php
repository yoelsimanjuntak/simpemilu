<?php
$rpemilu = $this->db
->where(COL_UNIQ, $id)
->get(TBL_TPEMILU)
->row_array();

$rkategori = $this->db
->query('select * from mkategori order by Uniq asc')
->result_array();

$rpartai = $this->db
->query('select * from mpartai order by ParNoUrut asc')
->result_array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url('site/pemilu/index')?>">PEMILIHAN</a></li>
          <li class="breadcrumb-item active"><?=$rpemilu[COL_JUDUL]?> - CALON</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <div class="card-header">
            <div class="card-tools text-center" style="float: none !important">
              <a href="<?=site_url('site/pemilu/calon-add/'.$id)?>" type="button" class="btn btn-tool btn-add text-primary"><i class="fas fa-plus"></i>&nbsp;TAMBAH DATA</a>
              <button type="button" class="btn btn-tool btn-refresh-data"><i class="fas fa-sync-alt"></i>&nbsp;REFRESH</button>
            </div>
          </div>
          <div class="card-body">
            <form id="dataform" method="post" action="#">
              <table id="datalist" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 10px">AKSI</th>
                    <th>NO. URUT</th>
                    <th>NAMA CALON</th>
                    <th>DAPIL</th>
                    <th>PARTAI</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div id="dom-filter-kategori" class="d-none">
  <select class="form-control" name="filterKategori" style="width: 300px">
    <?php
    foreach($rkategori as $kat) {
      ?>
      <option value="<?=$kat[COL_UNIQ]?>"><?=$kat[COL_KATDESC]?></option>
      <?php
    }
    ?>
  </select>
</div>
<div id="dom-filter-partai" class="d-none">
  <select class="form-control" name="filterPartai" style="width: 300px">
    <option value="">- SEMUA PARTAI -</option>
    <?php
    foreach($rpartai as $r) {
      ?>
      <option value="<?=$r[COL_UNIQ]?>"><?=str_pad($r[COL_PARNOURUT],2,"0",STR_PAD_LEFT)?>. <?=$r[COL_PARID]?></option>
      <?php
    }
    ?>
  </select>
</div>
<div class="modal fade" id="modal-form" role="dialog">
  <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">Form Calon</span>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
        <button type="submit" class="btn btn-sm btn-primary"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  var dt = $('#datalist').dataTable({
    "autoWidth" : false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?=site_url('site/pemilu/calon-load/'.$id)?>",
      "type": 'POST',
      "data": function(data){
        data.filterKategori = $('[name=filterKategori]', $('.filtering1')).val();
        data.filterPartai = $('[name=filterPartai]', $('.filtering2')).val();
       }
    },
    "scrollY" : '40vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    "oLanguage": {
      "sSearch": "FILTER "
    },
    //"aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    //"dom":"R<'row'<'col-sm-8 filtering'><'col-sm-4 text-right'f<'clear'>B>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    "dom":"R<'row'<'col-sm-12 d-flex'f<'filtering1'><'filtering2'>>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    "order": [],
    "columnDefs": [
      {"targets":[0], "className":'nowrap text-center'},
      {"targets":[1], "className":'nowrap dt-body-right'},
      {"targets":[2,3], "className":'nowrap'}
    ],
    "columns": [
      {"orderable": false,"width": "50px"},
      {"orderable": false,"width": "50px"},
      {"orderable": true},
      {"orderable": true},
      {"orderable": false}
    ],
    "createdRow": function(row, data, dataIndex) {
      $('.btn-action', $(row)).click(function() {
        var url = $(this).attr('href');
        if(confirm('Apakah anda yakin?')) {
          $.get(url, function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success(res.success);
            }
          }, "json").done(function() {
            dt.DataTable().ajax.reload();
          }).fail(function() {
            toastr.error('SERVER ERROR');
          });
        }
        return false;
      });
      $('.btn-edit', $(row)).click(function() {
        var href = $(this).attr('href');
        var modal = $('#modal-form');

        $('.modal-body', modal).html('<p class="text-center">MEMUAT...</p>');
        modal.modal('show');
        $('.modal-body', modal).load(href, function(){
          $("select", modal).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
        });
        return false;
      });
    },
    "initComplete": function(settings, json) {
      $('input[type=search]', $('#datalist_filter')).removeClass('form-control-sm').attr('placeholder', 'Keyword');
    }
  });
  $("div.filtering1").html($('#dom-filter-kategori').html()).addClass('d-inline-block ml-2');
  $("div.filtering2").html($('#dom-filter-partai').html()).addClass('d-inline-block ml-2');

  $('.btn-refresh-data').click(function() {
    dt.DataTable().ajax.reload();
  });
  $('input,select', $("div.filtering1")).change(function() {
    dt.DataTable().ajax.reload();
  }).trigger('change');
  $('input,select', $("div.filtering2")).change(function() {
    dt.DataTable().ajax.reload();
  }).trigger('change');

  $('.btn-add').click(function() {
    var href = $(this).attr('href');
    var modal = $('#modal-form');

    $('.modal-body', modal).html('<p class="text-center">MEMUAT...</p>');
    modal.modal('show');
    $('.modal-body', modal).load(href, function(){
      $("select", modal).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
    });
    return false;
  });

  $('button[type=submit]', $('#modal-form')).click(function(){
    var dis = $(this);
    var modal = $('#modal-form');
    dis.html("Loading...").attr("disabled", true);
    $('form', modal).ajaxSubmit({
      dataType: 'json',
      success : function(data){
        dt.DataTable().ajax.reload();
        if(data.error==0) {

        } else {
          toastr.error(data.error);
        }
      },
      complete: function(data) {
        modal.modal('hide');
        dis.html('<i class="far fa-check-circle"></i>&nbsp;SIMPAN').attr("disabled", false);
      }
    });
  });
});
</script>
