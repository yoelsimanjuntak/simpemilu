<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6 text-right">
        <p class="mb-0">
          <?=anchor('site/pemilu/add','<i class="far fa-plus-circle"></i> TAMBAH DATA',array('class'=>'btn btn-primary btn-sm btn-add'))?>
        </p>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <?php
      foreach($res as $r) {
        $arrdet = array();
        $rdet = $this->db
        ->join(TBL_MKATEGORI,TBL_MKATEGORI.'.'.COL_UNIQ." = ".TBL_TPEMILUDET.".".COL_IDKATEGORI,"inner")
        ->where(COL_IDPEMILU, $r[COL_UNIQ])
        ->get(TBL_TPEMILUDET)
        ->result_array();
        $rtps = $this->db
        ->where(COL_IDPEMILU, $r[COL_UNIQ])
        ->count_all_results(TBL_TPEMILUTPS);
        $rkandidat = $this->db
        ->where(COL_IDPEMILU, $r[COL_UNIQ])
        ->count_all_results(TBL_TKANDIDAT);
        foreach($rdet as $d) {
          $arrdet[]=$d[COL_KATNAMA];
        }
        ?>
        <div class="col-sm-6">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title"><?=$r[COL_JUDUL]?></h3>
              <div class="card-tools">
                <a href="<?=site_url('site/pemilu/edit/'.$r[COL_UNIQ])?>" class="btn btn-tool btn-sm btn-edit">
                  <i class="fas fa-edit"></i>
                </a>
                <a href="<?=site_url('site/pemilu/delete/'.$r[COL_UNIQ])?>" class="btn btn-tool btn-sm btn-delete">
                  <i class="fas fa-trash"></i>
                </a>
              </div>
            </div>
            <div class="card-body p-0">
              <table class="table table-striped table-valign-middle">
                <tbody>
                  <tr>
                    <td style="width: 100px; white-space: nowrap">Jadwal</td>
                    <td style="width: 10px">:</td>
                    <td class="font-weight-bold"><?=date('d-m-Y', strtotime($r[COL_TANGGAL]))?></td>
                  </tr>
                  <tr>
                    <td style="width: 100px; white-space: nowrap">Pemilihan</td>
                    <td style="width: 10px">:</td>
                    <td class="font-weight-bold"><?=!empty($arrdet)?implode($arrdet,', '):'-'?></td>
                  </tr>
                  <tr>
                    <td style="width: 100px; white-space: nowrap">Jumlah TPS</td>
                    <td style="width: 10px">:</td>
                    <td class="font-weight-bold"><?=number_format($rtps)?></td>
                  </tr>
                  <tr>
                    <td style="width: 100px; white-space: nowrap">Jumlah Calon</td>
                    <td style="width: 10px">:</td>
                    <td class="font-weight-bold"><?=number_format($rkandidat)?></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="card-footer">
              <a href="<?=site_url('site/pemilu/tps/'.$r[COL_UNIQ])?>" class="btn btn-info btn-sm"><i class="fas fa-box-ballot"></i>&nbsp; DATA TPS</a>
              <a href="<?=site_url('site/pemilu/calon/'.$r[COL_UNIQ])?>" class="btn btn-success btn-sm"><i class="fas fa-user-tie"></i>&nbsp; DATA CALON</a>
            </div>
          </div>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-form" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">Jadwal Pemilu</span>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
        <button type="submit" class="btn btn-sm btn-primary"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  $('button[type=submit]', $('#modal-form')).click(function(){
    var dis = $(this);
    dis.html("Loading...").attr("disabled", true);
    $('form', $('#modal-form')).ajaxSubmit({
      dataType: 'json',
      success : function(data){
        if(data.error==0) {
          location.reload();
        } else {
          toastr.error(data.error);
        }
      },
      complete: function(data) {
        dis.html('<i class="far fa-check-circle"></i>&nbsp;SIMPAN').attr("disabled", false);
      }
    });
  });
  $('.btn-add, .btn-edit').click(function() {
    var href = $(this).attr('href');
    var modal = $('#modal-form');

    $('.modal-body', modal).html('<p class="text-center">MEMUAT...</p>');
    modal.modal('show');
    $('.modal-body', modal).load(href, function(){
      $("select", modal).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
      $('.datepicker', modal).daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        maxYear: parseInt(moment().add(10, 'year').format('YYYY'),10),
        locale: {
            format: 'Y-MM-DD'
        }
      });
      $('.time-mask').inputmask({
        alias: "datetime",
        inputFormat: "HH:MM"
      });
    });
    return false;
  });
  $('.btn-delete').click(function() {
    var url = $(this).attr('href');
    if(confirm('Apakah anda yakin?')) {
      $.get(url, function(res) {
        if(res.error != 0) {
          toastr.error(res.error);
        } else {
          toastr.success(res.success);
          location.reload();
        }
      }, "json").done(function() {

      }).fail(function() {
        toastr.error('Maaf, telah terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi.');
      });
    }
    return false;
  });
});
</script>
