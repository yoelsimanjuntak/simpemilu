<?php
$arrdet = array();
if(!empty($data)) {
  $rdet = $this->db
  ->where(COL_IDKANDIDAT, $data[COL_UNIQ])
  ->get(TBL_TKANDIDATPARTAI)
  ->result_array();
  foreach($rdet as $d) {
    $arrdet[]=$d[COL_IDPARTAI];
  }
}

$rkategori = $this->db
->query('select * from mkategori order by Uniq asc')
->result_array();
?>
<form id="form-main" method="post" enctype="multipart/form-data" action="<?=current_url()?>">
  <div class="row">
    <div class="col-sm-7">
      <div class="form-group">
        <label>Kategori</label>
        <select class="form-control select2" name="<?=COL_IDKATEGORI?>" style="width: 100%">
          <?php
          foreach($rkategori as $kat) {
            ?>
            <option value="<?=$kat[COL_UNIQ]?>" data-kategori="<?=$kat[COL_KATNAMA]?>" <?=!empty($data)&&$data[COL_IDKATEGORI]==$kat[COL_UNIQ]?'selected':''?>><?=$kat[COL_KATDESC]?></option>
            <?php
          }
          ?>
        </select>
      </div>
    </div>
    <div class="col-sm-5 div-dapil">
      <div class="form-group">
        <label>Dapil</label>
        <select class="form-control select2" name="<?=COL_IDDAPIL?>" style="width: 100%">
          <?=GetCombobox("select * from mdapil order by Dapil", COL_UNIQ, COL_DAPIL, (!empty($data)?$data[COL_IDDAPIL]:null))?>
        </select>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-2">
      <div class="form-group">
        <label>No.</label>
        <input type="text" class="form-control" name="<?=COL_KANDNO?>" value="<?=!empty($data)?$data[COL_KANDNO]:''?>" placeholder="No. Urut" required />
      </div>
    </div>
    <div class="col-sm-5">
      <div class="form-group">
        <label>Nama Calon</label>
        <input type="text" class="form-control" name="<?=COL_KANDNAMA?>" value="<?=!empty($data)?$data[COL_KANDNAMA]:''?>" placeholder="Nama Calon" required />
      </div>
    </div>
    <div class="col-sm-5">
      <div class="form-group">
        <label>Nama Pasangan Calon</label>
        <input type="text" class="form-control" name="<?=COL_KANDNAMAWAKIL?>" value="<?=!empty($data)?$data[COL_KANDNAMAWAKIL]:''?>" placeholder="Khusus Pilpres & Pilkada" />
      </div>
    </div>
  </div>
  <div class="row mt-3">
    <div class="col-sm-7">
      <div class="row">
        <div class="col-sm-4 d-flex justify-content-center align-items-center">
          <div style="height: 5rem; width: 5rem; background-image: url('<?=!empty($data)&&!empty($data[COL_KANDFOTO])&&file_exists(MY_UPLOADPATH.'calon/'.$data[COL_KANDFOTO])?MY_UPLOADURL.'calon/'.$data[COL_KANDFOTO]:MY_IMAGEURL.'user.jpg'?>'); background-size: cover; border-radius: 15%"></div>
        </div>
        <div class="col-sm-8">
          <label>Foto</label>
          <div class="input-group">
            <div class="custom-file">
              <input type="file" class="custom-file-input" name="userfile" accept="image/*">
              <label class="custom-file-label" for="userfile">Unggah Gambar</label>
            </div>
          </div>
          <small class="text-muted font-italic">Ukuran maks. 2MB</small>
        </div>
      </div>
    </div>
    <div class="col-sm-5">
      <div class="form-group">
        <label>Partai</label>
        <select class="form-control select2" name="ArrPartai[]" style="width: 100%" multiple>
          <?=GetCombobox("select * from mpartai order by ParNoUrut", COL_UNIQ, array(COL_PARNOURUT, COL_PARID), (!empty($data)?$arrdet:null))?>
        </select>
      </div>
    </div>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function() {
  bsCustomFileInput.init();
  $('[name=IdKategori]', $('#form-main')).change(function(){
    $('.div-dapil', $('#form-main')).hide();
    $('input,select', $('.div-dapil', $('#form-main'))).val('');
    if($('option:selected', $('[name=IdKategori]', $('#form-main'))).data('kategori')=='DPRD-KAB-KOTA') {
      $('[name=IdDapil]', $('#form-main')).find('option').first().prop('selected', true).trigger('change');
      $('.div-dapil', $('#form-main')).show();
    }
  });
  <?php
  if(empty($data)) {
    ?>
    $('[name=IdKategori]', $('#form-main')).trigger('change');
    <?php
  } else {
    ?>
    if($('option:selected', $('[name=IdKategori]', $('#form-main'))).data('kategori')=='DPRD-KAB-KOTA') {
      $('.div-dapil', $('#form-main')).show();
    } else {
      $('.div-dapil', $('#form-main')).hide();
    }
    <?php
  }
  ?>
});
</script>
