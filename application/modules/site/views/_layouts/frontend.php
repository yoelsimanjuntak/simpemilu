<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <link rel="icon" type="image/png" href=<?=MY_IMAGEURL.$this->setting_web_logo?>>

  <title><?=!empty($title) ? $this->setting_web_name.' - '.$title : $this->setting_web_name?></title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?=base_url()?>assets/fonts/css/font-awesome.min.css" />
  <link rel="stylesheet" href="<?=base_url()?>assets/fonts/fontawesome-pro/web/css/all.min.css" />
  <!-- Ionicons -->
  <link href="<?=base_url()?>assets/fonts/css/ionicons.min.css" rel="stylesheet" type="text/css" />

  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/dist/css/adminlte.min.css">

  <!-- JQUERY -->
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/modernizr/modernizr.js"></script>

  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/iCheck/all.css">

  <!-- Select 2 -->
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

  <!-- DataTables -->
  <link rel="stylesheet" href="<?=base_url()?>assets/datatable/media/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/jquery.dataTables.min.js?ver=1"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/dataTables.bootstrap.min.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/dataTables.buttons.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.bootstrap.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.print.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.print.min.js"></script>
  <link href="<?=base_url()?>assets/datatable/ext/buttons/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?=base_url()?>assets/datatable/ext/responsive/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/jszip/jszip.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/pdfmake/build/pdfmake.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/pdfmake/build/vfs_fonts.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/responsive/js/dataTables.responsive.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.html5.min.js"></script>

  <!-- daterange picker -->
  <script src="<?=base_url()?>assets/js/moment.js"></script>
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">

  <!-- Toastr -->
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.css">
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.js"></script>

  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">

  <style>
  .no-js #loader { display: none;  }
  .js #loader { display: block; position: absolute; left: 100px; top: 0; }
  .se-pre-con {
      position: fixed;
      left: 0px;
      top: 0px;
      width: 100%;
      height: 100%;
      z-index: 9999;
      background: url(<?=base_url()?>assets/media/preloader/<?=$this->setting_web_preloader?>) center no-repeat #fff;
  }

  @media (max-width: 767px) {
      .sidebar-toggle {
          font-size: 3vw !important;
      }
  }

  .form-group .control-label {
      text-align: right;
      line-height: 2;
  }
  .nowrap {
    white-space: nowrap;
  }
  .va-middle {
    vertical-align: middle !important;
  }
  .border-0 td {
    border: 0!important;
  }
  td.dt-body-right {
    text-align: right !important;
  }
  .custom-file-label {
    overflow-x: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    padding-right: 75px;
  }
  .form-group .control-label {
    line-height: 1.5 !important;
  }
  .table td, .table th {
    vertical-align: middle !important;
  }
  .table.table-condensed td, .table.table-condensed th {
    padding: .25rem !important;
    padding-right: 1rem !important;
    padding-left: 1rem !important;
  }
  </style>

  <script>
  $(window).load(function() {
    $(".se-pre-con").fadeOut("slow");
  });
  </script>
</head>
<body class="hold-transition layout-top-nav">
  <div class="se-pre-con"></div>
  <div class="wrapper">

    <!-- Header -->
    <nav class="main-header navbar navbar-expand navbar-light navbar-white">
      <div class="container">
        <a href="<?=site_url()?>" class="navbar-brand">
          <img src="<?=MY_IMAGEURL.$this->setting_web_logo?>" alt="Logo" class="brand-image" style="opacity: .8" />
          <span class="brand-text"><strong><?=$this->setting_web_name?></strong> <span class="d-none d-sm-inline font-weight-light"><?=$this->setting_org_name?></span></span>
        </a>
      </div>
    </nav>
    <!-- Header -->

    <div class="content-wrapper">
      <?=$content?>
    </div>

    <footer class="main-footer d-flex" style="justify-content: space-between">
      <div>
        <strong>Copyright &copy; <?=date("Y")?> <?=$this->setting_web_name?></strong>. <span class="d-none d-sm-inline">Strongly developed by <b>Partopi Tao</b>.</span>
      </div>
      <div class="text-right d-none d-sm-inline">
        <button type="button" class="btn btn-xs btn-primary"><i class="fas fa-calendar"></i>&nbsp;<span id="date"></span></button>&nbsp;
        <button type="button" class="btn btn-xs btn-success"><i class="fas fa-clock"></i>&nbsp;<span id="time"></span></button>
      </div>
    </footer>
  </div>

  <!-- Bootstrap -->
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- AdminLTE App -->
  <script src="<?=base_url()?>assets/themes/adminlte-new/dist/js/adminlte.js"></script>

  <!-- ChartJS -->
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/chart.js.old/Chart.min.js"></script>

  <!-- FastClick -->
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/fastclick/fastclick.js"></script>
  <!-- Sparkline -->
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/sparklines/sparkline.js"></script>
  <!-- SlimScroll 1.3.0 -->
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/slimScroll/jquery.slimscroll.min.js"></script>
  <!-- iCheck 1.0.1 -->
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/iCheck/icheck.min.js"></script>
  <!-- Select 2 -->
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/select2/js/select2.full.min.js"></script>
  <!-- Bootstrap select -->
  <script src="<?=base_url()?>assets/js/bootstrap-select.js"></script>
  <!-- Upload file -->
  <!--<script src="<?=base_url()?>assets/js/jquery.uploadfile.min.js"></script>-->

  <!-- Block UI -->
  <script type="text/javascript" src="<?=base_url() ?>assets/js/jquery.blockUI.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.validate.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/js/function.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.form.js"></script>

  <!-- date-range-picker -->
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/daterangepicker/daterangepicker.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>

  <!-- JSPDF -->
  <script src="<?=base_url()?>assets/js/jspdf/jspdf.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/js/jspdf/libs/png_support/zlib.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/js/jspdf/libs/png_support/png.js"></script>
  <script src="<?=base_url()?>assets/js/jspdf/plugins/from_html.js"></script>
  <script src="<?=base_url()?>assets/js/jspdf/plugins/addimage.js"></script>
  <script src="<?=base_url()?>assets/js/jspdf/plugins/png_support.js"></script>
  <script src="<?=base_url()?>assets/js/jspdf/plugins/split_text_to_size.js"></script>
  <script src="<?=base_url()?>assets/js/jspdf/plugins/standard_fonts_metrics.js"></script>
  <script src="<?=base_url()?>assets/js/jspdf/plugins/filesaver.js"></script>

  <!-- HTML2CANVAS -->
  <script src="<?=base_url()?>assets/js/html2canvas.js"></script>

  <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.number.js"></script>

  <!-- InputMask -->
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/inputmask/jquery.inputmask.bundle.js"></script>

  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>

  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/inputmask/jquery.inputmask.bundle.js"></script>

  <script type="text/javascript">
  function startTime() {
    var today = new Date();
    $('#date').html(moment(today).format('DD')+' '+moment(today).format('MMMM').toUpperCase().substr(0, 3)+' '+moment(today).format('Y'));
    $('#time').html(moment(today).format('hh')+':'+moment(today).format('mm')+':'+moment(today).format('ss'))
    var t = setTimeout(startTime, 1000);
  }
  $(document).ready(function(){
    startTime();
    $('[data-mask]').inputmask();
    $('a[href="<?=current_url()?>"]').addClass('active');
    $('.dropdown-menu textarea,.dropdown-menu input, .dropdown-menu label').click(function(e) {
      e.stopPropagation();
    });
    $(document).on('keypress','.angka',function(e) {
      if((e.which <= 57 && e.which >= 48) || (e.keyCode >= 37 && e.keyCode <= 40) || e.keyCode==9 || e.which==43 || e.which==44 || e.which==45 || e.which==46 || e.keyCode==8){
        return true;
      } else {
        return false;
      }
    });
    $(document).on('blur','.uang',function(){
      $(this).val(desimal($(this).val(),0));
    }).on('focus','.uang',function(){
      $(this).val(toNum($(this).val()));
    });
    $(".uang").trigger("blur");
    $('a[href="<?=current_url()?>"]').addClass('active');
    $('a[href="<?=current_url()?>"]').closest('li.has-treeview').addClass('menu-open').children('.nav-link').addClass('active');
    $("select").not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
    $('.datepicker').daterangepicker({
      singleDatePicker: true,
      showDropdowns: true,
      maxYear: parseInt(moment().add(10, 'year').format('YYYY'),10),
      locale: {
        format: 'Y-MM-DD'
      }
    });
    $('.datepicker').on('show.daterangepicker', function(ev, picker) {
      console.log("calendar is here");
    });
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });

    $(".money").number(true, 2, '.', ',');
    $(".uang").number(true, 0, '.', ',');
  });
  </script>
</body>
